@extends('layout')
@section('title','<i class="fa fa-th-large"></i> Template Builder')

@section('toolbar')
<a href="{{ route('builder.editor') }}" class="btn blue"><i class="fa fa-plus"></i> Novo Template</a>
@endsection

@push('js_footer')
<script src="/vendor/datatables/datatables.min.js"></script>
<script src="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
@endpush

@push('style_header')
<link href="/vendor/datatables/datatables.css" rel="stylesheet" type="text/css" />
<link href="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

<table class="datatable table table-bordered table-striped table-hover table-responsive" width="100%" data-stateSave="true" data-paging="true">
    <thead>
        <tr>
            <th width="25%">Nome</th>
            <th width="40%">Descrição</th>
            <th width="10%" class="text-center">Publicado</th>
            <th width="20%">Preview</th>
            <th width="5%" class="text-center">Ação</th>
        </tr>
    </thead>
    <tbody>
        @foreach($templates as $template)
        <tr>
            <td>
                <a href="{{ route('builder.editor', ['id_template' => $template->id_template]) }}">{{ $template->nome }}</a>
            </td>
            <td>{{ $template->descricao }}</td>
            <td align="center" class="text-center {{ $template->publicado == 'S' ? 'text-success' : 'text-danger' }}"><i class="fa {{ $template->publicado == 'S' ? 'fa-check' : 'fa-close' }}"></i></td>
            <td><img class="img-responsive" src="{{ $template->thumbnail }}" /></td>
            <td nowrap="nowrap">
                <a href="{{ route('builder.editor', ['id_template' => $template->id_template]) }}" class="btn btn-sm blue tooltips" data-placement="top" data-original-title="Editar" @enabledIf("TEMPLATE","PODE_EDITAR")><i class="fa fa-pencil"></i></a>&nbsp;
                <a href="javascript:;" class="btn btn-sm red tooltips btn-destroy" data-url="{{ route('builder.destroy', ['id_template' => $template->id_template]) }}" data-placement="top" data-original-title="Excluir" data-message="Deseja realmente excluir permanentemente este template?" data-success-message="Template excluído com sucesso!" @enabledIf("TEMPLATE","PODE_EXCLUIR")><i class="fa fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
