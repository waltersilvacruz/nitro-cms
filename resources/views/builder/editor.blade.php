@extends('layout')
@section('title','<i class="fa fa-th-large"></i> Template Builder <small>/ novo</small>')

@push('js_footer')
<script src="/components/jquery-ui/jquery-ui.min.js"></script>
<script src="/components/gridstack/dist/gridstack.min.js"></script>
<script src="/components/html2canvas/build/html2canvas.min.js"></script>
<script src="/js/builder/template-builder.min.js"></script>
@endpush

@push('style_header')
<link href="/components/gridstack/dist/gridstack.min.css" rel="stylesheet" type="text/css" />
@endpush

@section('content')

<div class="row">
    <div class="col-md-2">
        <div class="grid-toolbar block-mode toolbar-bloco trash ui-droppable text-center disabled">
            <img src="/img/builder-trash.png" /><br />
            Arraste e solte os blocos<br />aqui para removê-los.
        </div>
        <div class="grid-toolbar edit-mode toolbar-edicao" style="display: none;">
            <div class="panel-group accordion" id="config-accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#config-accordion" href="#config-layout" aria-expanded="true"><i class="fa fa-columns"></i> Layout </a>
                        </h4>
                    </div>
                    <div id="config-layout" class="panel-collapse collapse in" aria-expanded="true">
                        <div class="panel-body builder-panel-layout no-margin no-padding" style="height:400px; overflow-y:auto;">
                            <p class="bg-red item-not-selected no-margin" style="padding: 10px;"><i class="fa fa-exclamation-circle"></i> Selecione um bloco!</p>
                            <div id="layouts-container">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#config-accordion" href="#config-image" aria-expanded="false"><i class="fa fa-image"></i> Imagem </a>
                        </h4>
                    </div>
                    <div id="config-image" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body no-margin no-padding">
                            <p class="bg-red item-not-selected no-margin" style="padding: 10px;"><i class="fa fa-exclamation-circle"></i> Selecione um bloco!</p>
                            <div class="block-unselected">
                                <form role="form">
                                    <div class="force-padding-10">
                                        <div class="form-group">
                                            <label class="control-label">Estilo</label>
                                            <select class="form-control" id="image_style" data-type="image" data-control="true" data-event="change" data-action="css-class" data-target=".data-image">
                                                <option value="">Nenhum</option>
                                                <option value="border-radius-5">Cantos arredondados (5px)</option>
                                                <option value="border-radius-10">Cantos arredondados (10px)</option>
                                                <option value="border-radius-15">Cantos arredondados (15px)</option>
                                                <option value="border-radius-20">Cantos arredondados (20px)</option>
                                                <option value="border-radius-25">Cantos arredondados (25px)</option>
                                                <option value="border-radius-30">Cantos arredondados (30px)</option>
                                                <option value="border-radius-35">Cantos arredondados (35px)</option>
                                                <option value="border-radius-40">Cantos arredondados (40px)</option>
                                                <option value="border-radius-45">Cantos arredondados (45px)</option>
                                                <option value="border-radius-50">Cantos arredondados (50px)</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Borda</label>
                                            <select class="form-control" id="image_border" data-type="image" data-control="true" data-event="change" data-action="css-class" data-target=".data-image">
                                                <option value="">Nenhuma</option>
                                                <option value="border-1">1 pixel</option>
                                                <option value="border-2">2 pixels</option>
                                                <option value="border-3">3 pixels</option>
                                                <option value="border-4">4 pixels</option>
                                                <option value="border-5">5 pixels</option>
                                            </select>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#config-accordion" href="#config-text" aria-expanded="false"><i class="fa fa-font"></i> Texto </a>
                        </h4>
                    </div>
                    <div id="config-text" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body no-margin no-padding">
                            <p class="bg-red item-not-selected no-margin" style="padding: 10px;"><i class="fa fa-exclamation-circle"></i> Selecione um bloco!</p>
                            <div class="block-unselected">
                                <form role="form">
                                    <div class="force-padding-10">

                                        <div class="form-group">
                                            <label class="control-label">Alinhamento do texto</label>
                                            <select class="form-control" id="text_align" data-type="align" data-control="true" data-event="change" data-action="css-class" data-target=".grid-content">
                                                <option value="">Padrão do template</option>
                                                <option value="text-left">Esquerda</option>
                                                <option value="text-right">Direita</option>
                                                <option value="text-center">Centralizado</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Chapéu</label>
                                            <select class="form-control" id="text_header" data-type="header" data-control="true" data-event="change" data-action="css-class" data-target=".data-header">
                                                <option value="">Mostrar</option>
                                                <option value="hide">Ocultar</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Título</label>
                                            <select class="form-control" id="text_title" data-type="title" data-control="true" data-event="change" data-action="css-class" data-target=".data-title">
                                                <option value="">Mostrar</option>
                                                <option value="hide">Ocultar</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Estilo do título</label>
                                            <select class="form-control" id="text_title_style" data-type="title" data-control="true" data-event="change" data-action="tag-replace" data-target=".data-title">
                                                <option value="">Padrão do template</option>
                                                <option value="h1">H1</option>
                                                <option value="h2">H2</option>
                                                <option value="h3">H3</option>
                                                <option value="h4">H4</option>
                                                <option value="h5">H5</option>
                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Complemento</label>
                                            <select class="form-control" id="text_description" data-type="description" data-control="true" data-event="change" data-action="css-class" data-target=".data-description">
                                                <option value="">Mostrar</option>
                                                <option value="hide">Ocultar</option>
                                            </select>
                                        </div>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-10">
        <div class="template-container margin-top-20" id="template_container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="toolbar-bloco">
                        <select class="form-control-static" id="largura">
                            <option value="3">3 colunas</option>
                            <option value="4" selected>4 colunas</option>
                            <option value="5">5 colunas</option>
                            <option value="6">6 colunas</option>
                            <option value="7">7 colunas</option>
                            <option value="8">8 colunas</option>
                            <option value="9">9 colunas</option>
                            <option value="12">12 colunas</option>
                        </select>
                        X
                        <select class="form-control-static" id="altura">
                            <option value="2">2 linhas</option>
                            <option value="3">3 linhas</option>
                            <option value="4" selected>4 linhas</option>
                            <option value="5">5 linhas</option>
                            <option value="6">6 linhas</option>
                            <option value="7">7 linhas</option>
                            <option value="8">8 linhas</option>
                            <option value="9">9 linhas</option>
                            <option value="10">10 linhas</option>
                            <option value="11">11 linhas</option>
                            <option value="12">12 linhas</option>
                        </select>
                        <a class="btn btn-link" id="btn-add-block" href="javascript:;"> Adicionar Bloco <i class="fa fa-lg fa-arrow-circle-down"></i></a>
                    </span>
                    <span class="toolbar-edicao text-info">
                        <strong><i class="fa fa-eye"></i> Textos e imagens dos layouts são meramente ilustrativos</strong>
                    </span>
                    <span class="pull-right">
                        Visualizar:&nbsp;
                        <div class="btn-group" role="group" aria-label="...">
                            <button type="button" data-mode="bloco" class="btn blue-madison mode"><i class='fa fa-object-group'></i> Blocos</button>
                            <button type="button" data-mode="edicao" disabled="disabled" class="btn default mode"><i class='fa fa-pencil-square-o'></i> Edição</button>
                        </div>
                    </span>
                </div>
            </div>
            <div class="template-container-target">
                <div class="template-border-container">
                    <div class="grid-stack" id="grid-stack-template-container">

                    </div>
                </div>
            </div>
            <br /><br />
            <form role="form" class="toolbar-edicao">
                @if($id_template)
                <input type="hidden" name="id_template" id="id_template" value="{{ $id_template }}" />
                @endif
                <input type="hidden" name="_token" id="_token" value="{{ $token }}" />
                
                <div class="row">
                    <div class="col-xs-10">
                        <div class="form-group">
                            <label class="control-label"><strong>Nome do Template</strong></label>
                            <input type="text" class="form-control" name="nome" id="nome" value="" placeholder="Ex: Meu template" />
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <span class="pull-right">
                            <label class="control-label"><strong>Publicado</strong></label><br />
                            <input type="checkbox" class="form-control switch" id="publicado" name="publicado" @if($template) @if($template->publicado == 'S') checked @endif @endif value="S" data-on-color="info" data-off-color="danger" data-on-text="Sim" data-off-text="Não">
                            </span>
                        </div>
                    </div>
                </div>
<!--                <div class="form-group">
                    
                    
                </div>-->

                <div class="form-group">
                    <label class="control-label">Descrição</label>
                    <textarea class="form-control" name="descricao" id="descricao" rows="3" placeholder="Descrição do seu template"></textarea>
                </div>
            </form>
        </div>


        <p>
            <a class="btn red" id="btn-cancelar" href="{{ route('builder.index') }}"><i class="fa fa-close"></i> Cancelar e voltar</a>
            <a class="btn green" id="btn-salvar" href="javascript:;"><i class="fa fa-check"></i> Salvar template</a>
        </p>
    </div>
</div>
@include('builder.script-templates')
@stop
