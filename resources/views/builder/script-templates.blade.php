<script type="text/template" id="layout_template">
<%_.each(data, function(item) { %>
<a href="#" data-id="<%- item.id %>" data-role="template" class="bg-white">
    <div class="panel-list-item-content block-unselected blocked" 
        data-toogle="blockSelected" 
        data-min-width="<%- item.restrictions.width.min %>"  
        data-max-width="<%- item.restrictions.width.max %>"
        data-min-height="<%- item.restrictions.height.min %>"  
        data-max-height="<%- item.restrictions.height.max %>"
        data-min-aspect-ratio="<%- item.restrictions.aspectRatio.min %>"  
        data-max-aspect-ratio="<%- item.restrictions.aspectRatio.max %>">
        <span class="pull-right"><img src="<%- item.thumbnail %>" class="layout-thumb" /></span>
        <p class="text-uppercase"><strong><%- item.name %></strong></p>
        <p><%- item.description %></p>
    </div>
</a>
<% }) %>
</script>