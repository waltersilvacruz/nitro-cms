<!-- BEGIN SIDEBAR MENU -->
<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->

<ul class="page-sidebar-menu  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <li class="sidebar-toggler-wrapper hide">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler"> </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
    </li>
    <li class="sidebar-search-wrapper">
        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
        <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
        <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
        {!! Form::open(['route' => array('home'), 'class' => 'sidebar-search', 'method' => 'GET']) !!}
            <a href="javascript:;" class="remove">
                <i class="icon-close"></i>
            </a>
            <div class="input-group">
                <input type="text" class="form-control" name="q" id="q" placeholder="Procurar...">
                <span class="input-group-btn">
                    <button type="button" class="btn submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        {!! Form::close() !!}
        <!-- END RESPONSIVE QUICK SEARCH FORM -->
    </li>
    <li class="heading">
        <h3 class="uppercase">Home</h3>
    </li>
    <li class="nav-item">
        <a href="{{ route('home') }}" class="nav-link ">
            <i class="fa fa-pie-chart"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>
    <li class="heading">
        <h3 class="uppercase">Redação</h3>
    </li>
    <li class="nav-item">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-file-text"></i>
            <span class="title">Matérias e Páginas</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a href="{{ route('secao') }}" class="nav-link">
                    <i class="fa fa-file-text"></i>
                    <span class="title">Gerenciar Matérias</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('menu') }}" class="nav-link ">
                    <i class="fa fa-file-o"></i>
                    <span class="title">Gerenciar Páginas</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link ">
                    <i class="fa fa-star"></i>
                    <span class="title">Capa do Portal</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item  ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-quote-left"></i>
            <span class="title">Artigos</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a href="{{ route('artigo') }}" class="nav-link ">
                     <i class="fa fa-quote-right"></i>
                    <span class="title">Cadastro de Artigos</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('artigo.articulista') }}" class="nav-link ">
                     <i class="fa fa-users"></i>
                    <span class="title">Cadastro de Articulistas</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item">
        <a href="javascript:;" class="nav-link ">
            <i class="fa fa-picture-o"></i>
            <span class="title">Galerias de Fotos</span>
        </a>
    </li>

    <li class="nav-item  ">
        <a href="{{ route('webdisco') }}" class="nav-link nav-toggle">
            <i class="fa fa-folder-open"></i>
            <span class="title">Webdisco</span>
        </a>
    </li>
    
    <li class="heading">
        <h3 class="uppercase">Administração</h3>
    </li>
    <li class="nav-item">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="fa fa-lock"></i>
            <span class="title">Controle de Acessos</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item">
                <a href="javascript:;" class="nav-link">
                    <i class="fa fa-users"></i>
                    <span class="title">Grupos de Acesso</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link ">
                    <i class="fa fa-user"></i>
                    <span class="title">Cadastro de Usuários</span>
                </a>
            </li>
        </ul>
    </li>
    
    <li class="nav-item">
        <a href="{{ route('builder.index') }}" class="nav-link ">
            <i class="fa fa-th-large"></i>
            <span class="title">Template Builder</span>
        </a>
    </li>


</ul>
<!-- END SIDEBAR MENU -->
