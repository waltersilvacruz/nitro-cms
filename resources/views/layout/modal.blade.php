<!-- confirmações, prompts & alertas -->
<div class="modal fade" id="message-modal" tabindex="-1" role="dialog" aria-labelledby="message-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modal-title"></h4>
            </div>
            <div class="modal-body" id='modal-body'>
                <div id="modal-message"></div>
                <div id="modal-form" style="display: none;">
                    <div class="form-horizontal">
                        <div class="form-body">
                            <div class="form-group" id="modal-form-group">
                                <label class="col-md-3 control-label" id="modal-label"></label>
                                <div class="col-md-9">
                                    <div class="input-icon right">
                                        <i class="fa fa-warning tooltips" id="modal-input-icon" style="display: none;" data-original-title="Este campo é obrigatório!"></i>
                                        <input type="text" class="form-control" placeholder="" name="input" id="modal-input"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="modal-btn-cancel"><i class="fa fa-close"></i> Fechar</button>
                <button type="button" class="btn btn-success" id="modal-btn-continue"><i class="fa fa-check"></i> Confirmar</button>
            </div>
        </div>
    </div>
</div>

<!-- popup-iframe -->
<div class="modal fade" id="iframe-modal" tabindex="-1" aria-labelledby="xmessage-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="pull-right" id="iframe-modal-close-button"><a class="btn btn-link text-muted btn-lg" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a></span>
                <h4 class="modal-title" id="iframe-title"></h4>
            </div>
            <div class="modal-body" id='iframe-body'>
                <iframe frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>