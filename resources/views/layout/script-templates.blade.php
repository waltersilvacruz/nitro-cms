<script type='text/template' id='webdisco_attachment'>
    <table class="table table-hover table-light">
        <thead>
            <tr>
                <th class="text-left" colspan="2">Arquivo</th>
                <th class="text-left">Código</th>
                <th class="text-left">Autor</th>
                <th class="text-left">Tamanho</th>
                <th class="text-left">Data</th>
                <th class="text-center">Ação</th>
            </tr>
        </thead>
        <tbody>
            <% _.each(data.items, function(item){ %>
            <tr class="webdisco-file-item sidebar">
                <td width="64">
                <% if(item.arquivo_extensao === 'jpg' || item.arquivo_extensao === 'jpeg') { %>
                     <div class="webdisco-thumbnail webdisco-thumbnail-small" style="min-width: 64px; background:url(<%- item.thumbnail_small %>) no-repeat center center;"></div>
                <% } else { %>
                    <div class="file-icon-background-small" data-extension="<%- item.arquivo_extensao %>">
                        <div class="file-icon-extension"><%- item.arquivo_extensao %></div>
                    </div>
                <% } %>
                </td>
                <td width="55%"><%- item.titulo %></td>
                <td width="5%" nowrap>#<%- item.id_arquivo %></td>
                <td width="10%"><%- item.created_by %></td>
                <td width="10%"><%- item.arquivo_tamanho %></td>
                <td width="20%"><%- item.data_formatada %></td>
                <td width="1%" class="text-center"><a href="javascript:;" data-target="<%- data.target %>" data-id="<%- item.id_arquivo %>" class="btn btn-default webdisco-attachment-delete"><i class="fa fa-trash"></i></a></td>
            </tr>
            <% }) %>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="7"><%- data.items.length %> arquivos selecionados</th>
            </tr>
        </tfoot>
    </thead>
</script>

<script type='text/template' id='webdisco_inline_image'>
    <div class="webdisco-inline-item">
        <span class="webdisco-badge"><a href="javascript:;" data-target="<%- data.target %>" data-id="<%- data.item.id_arquivo %>" class="btn btn-default webdisco-inline_image-delete"><i class="fa fa-trash"></i></a></span>
        <img src="<%- data.item.thumbnail %>" border="0">
    </div>
</script>