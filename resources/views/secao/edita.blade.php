@extends('layout')
@section('title')
<i class="fa fa-file-text"></i> Gerenciar Matérias <small>/ editar seção</small>
@endsection

@section('content')
<form method="put" id="pagina-form" class="form-horizontal ajax-form" data-success="Seção atualizada com sucesso!" data-redirect="{{ route('secao') }}" data-mark-field="true" role="form" action="{{ route('secao.update', ['id_secao' => $secao->id_secao]) }}">
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
    
    <div class="alert alert-danger form-error-display"></div>
    
    <div class="form-group">
        <label class="control-label col-md-2"><strong>Nome da seção</strong></label>
        <div class="col-md-4">
            <input type="text" class="form-control" maxlength="100" name="nome" id="nome" placeholder="Ex: Portal do Cidadão" value="{{ $secao->nome }}" />
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-md-2">Descrição</label>
        <div class="col-md-7">
            <textarea class="form-control" rows="4" name="descricao" id="descricao">{{ $secao->descricao }}</textarea>
        </div> 
    </div>
    
    <div class="form-group">
        <label class="control-label col-md-2"><strong>Segurança</strong></label>
        <div class="col-md-5">
            <input type="checkbox" class="form-control switch" id="publico" @if($secao->publico == 'S') checked @endif name="publico" value="S" data-on-color="info" data-off-color="danger" data-on-text="Público" data-off-text="Restrito">
        </div> 
    </div>
    
    <div class="form-group">
        <label class="control-label col-md-2"><strong>Publicado</strong></label>
        <div class="col-md-5">
            <input type="checkbox" class="form-control switch" id="publicado" name="publicado" @if($secao->publicado == 'S') checked @endif value="S" data-on-color="info" data-off-color="danger" data-on-text="Sim" data-off-text="Não">
        </div> 
    </div>
    
    
    
    <hr />
    <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>
        <div class="col-md-10">
            <a href="{{ route('secao') }}" class="btn red"><i class="fa fa-close"></i> Cancelar</a>&nbsp;
            <button type="submit" id="btn-salvar" class="btn green"><i class="fa fa-check"></i> Salvar</button>
        </div>
    </div>
</form>
@endsection