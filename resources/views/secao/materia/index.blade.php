@extends('layout')
@section('title')
<i class="fa fa-file-text"></i> Gerenciar Matérias <small>/ {{ $secao->nome }} / lista de matérias</small>
@endsection

@push('js_footer')
<script src="/vendor/datatables/datatables.min.js"></script>
<script src="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
<script src="/js/secao/materia/index.min.js"></script>
@endpush

@push('style_header')
<link href="/vendor/datatables/datatables.css" rel="stylesheet" type="text/css" />
<link href="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush

@section('toolbar')
<a href="{{ route('secao') }}" class="btn default"><i class="fa fa-list"></i> Ver todos as seções</a>
<a href="{{ route('secao.materia.novo', ['id_secao' => $id_secao]) }}" class="btn blue"><i class="fa fa-plus"></i> Nova Matéria</a>
@endsection

@section('content')
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
<input type="hidden" name="_paging_url" id="_paging_url" value="{{ route('secao.materias', ['id_secao' => $id_secao]) }}" />

<table id="materias_table" 
       class="datatable table table-bordered table-striped table-hover table-responsive" 
       width="100%" 
       data-paging="true" 
       data-stateSave="true" 
       data-server-side="true" 
       data-ajax="{{ route('secao.materias', ['id_secao' => $id_secao]) }}" 
       data-order='[[0, "desc"]]'
       data-columns='[{"data": "data_materia"},{"data": "titulo"},{"data": "created_by"},{"data": "publicado"},{"data": "acao"}]'>
    <thead>
        <tr>
            <th width="10%">Data</th>
            <th width="70%">Título</th>
            <th width="10%">Autor</th>
            <th width="5%" class="text-center">Publicado</th>
            <th width="5%" class="text-center">Ação</th>
        </tr>
    </thead>
</table>
@endsection