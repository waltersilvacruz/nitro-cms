@extends('layout')
@section('title')
<i class="fa fa-file-text"></i> Gerenciar Matérias <small>/ {{ $secao->nome }} / editar matéria</small>
@endsection

@push('js_footer')
<script src="/vendor/ckeditor/ckeditor.js"></script>
<script src="/js/secao/materia/form.min.js"></script>
@endpush

@section('content')
<form method="put" id="materia-form" class="ajax-form" data-success="Matéria atualizada com sucesso!" data-redirect="{{ route('secao.materia', ['id_secao' => $secao->id_secao]) }}" data-mark-field="true" role="form" action="{{ route('secao.materia.update', ['id_secao' => $secao->id_secao, 'id_materia' => $materia->id_materia]) }}">
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

    <div class="row">
        <div class="col-xs-4">
            <div  id="treeview-container" class="scroller-y full-height">
                <div class="form-group">
                    <label class="control-label"><strong>Título da matéria</strong></label>
                    <input type="text" name="titulo" id="titulo" maxlength="100" class="form-control" value="{{ $materia->titulo }}" />
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <strong>Data</strong>
                    </div>
                    <div class="col-xs-6">
                        <strong>Hora</strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-inline datepicker" name="data" id="data" maxlength="10" value="{{ \App\Helpers\UtilHelper::formatDate($materia->data_materia, false) }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar-check-o"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker timepicker-24" name="hora" id="hora" maxlength="4" value="{{ substr(\App\Helpers\UtilHelper::formatDate($materia->data_materia, true),11,5) }}">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-clock-o"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <br />
                <div class="form-group">
                    <label class="control-label">Chapéu</label>
                    <input type="text" name="chapeu" id="chapeu" maxlength="50" class="form-control" value="{{ $materia->chapeu }}"/>
                </div>

                <div class="form-group">
                    <label class="control-label">Complemento</label>
                    <textarea class="form-control" name="complemento" id="complemento" rows="2">{{ $materia->complemento }}</textarea>
                </div>

                <div class="form-group">
                    <label class="control-label">Tags</label>
                    <input type="text" class="form-control" name="tags" id="tags" value="{{ $tags }}" />
                </div>
                
                <div class="form-group">
                    <label class="control-label">Publicado</label><br />
                    <input type="checkbox" class="form-control" id="publicado" name="publicado" value="S" @if($materia->publicado == 'S') checked @endif data-on-color="info" data-off-color="danger" data-on-text="Sim" data-off-text="Não">
                </div>

                <br /><br />
                <div class="form-group">
                    <a type="button" href="{{ route('secao.materia', ['id_secao' => $secao->id_secao]) }}" id="btn-cancelar" class="btn red"><i class="fa fa-close"></i> Cancelar</a>&nbsp;
                    <button type="button" id="btn-salvar" class="btn green"><i class="fa fa-check"></i> Salvar matéria</button>
                </div>

            </div>
        </div>
        <div class="col-xs-8">
            <div id="pagina-container" class="scroller-xy full-height" style="border-left: 1px solid #e7ecf1">

                <div style="width: 98%">
                    <div class="row" style="padding-left: 20px;">
                        <div class="col-xs-12">
                            <div class="form-group" id="editor-field">
                                <div id="editor-ruler"></div>
                                <div id="ckeditorToolbar"></div>
                                <div id="toolbar-placeholder" style="display: none;"></div>
                                <textarea class="form-control" name="texto" id="texto">{{ $materia->texto }}</textarea>
                                <div id="toolbar-endline"></div>
                            </div>

                            <br />
                            <div class="form-group">
                                <label class="control-label">Anexos</label>
                                <webdisco data-type="attachment" data-input-name="anexos" data-input-value="{{ $anexos }}" data-title="Selecionar arquivos"></webdisco>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</form>
@endsection