@extends('layout')
@section('title')
<i class="fa fa-file-text"></i> Gerenciar Matérias
@endsection

@push('js_footer')
<script src="/vendor/datatables/datatables.min.js"></script>
<script src="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
@endpush

@push('style_header')
<link href="/vendor/datatables/datatables.css" rel="stylesheet" type="text/css" />
<link href="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush

@section('toolbar')
<a href="{{ route('secao.novo') }}" class="btn blue"><i class="fa fa-plus"></i> Nova Seção de Matérias</a>
@endsection

@section('content')
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

<table class="datatable table table-bordered table-striped table-hover table-responsive" width="100%" data-stateSave="true" data-paging="true">
    <thead>
        <tr>
            <th width="35%">Seção</th>
            <th width="40%">Descrição</th>
            <th width="10%" class="text-center">Público</th>
            <th width="10%" class="text-center">Publicado</th>
            <th width="5%" class="text-center">Ação</th>
        </tr>
    </thead>
    <tbody>
        @foreach($secoes as $secao)
        <tr>
            <td>
                <i class="fa fa-list"></i> <a href="{{ route('secao.materia', ['id_secao' => $secao->id_secao]) }}"><strong>{{ $secao->nome }}</strong></a>
            </td>
            <td>{{ $secao->descricao }}</td>
            <td align="center" class="text-center {{ $secao->publico == 'S' ? 'text-success' : 'text-danger' }}"><i class="fa {{ $secao->publico == 'S' ? 'fa-check' : 'fa-close' }}"></i></td>
            <td align="center" class="text-center {{ $secao->publicado == 'S' ? 'text-success' : 'text-danger' }}"><i class="fa {{ $secao->publicado == 'S' ? 'fa-check' : 'fa-close' }}"></i></td>
            <td nowrap="nowrap">
                <a href="{{ route('secao.materia', ['id_secao' => $secao->id_secao]) }}" class="btn btn-sm blue tooltips" data-placement="top" data-original-title="Matérias" @enabledIf("MATERIA","PODE_ACESSAR")><i class="fa fa-copy"></i></a>&nbsp;
                <a href="{{ route('secao.edita', ['id_secao' => $secao->id_secao]) }}" class="btn btn-sm blue tooltips" data-placement="top" data-original-title="Editar" @enabledIf("SECAO","PODE_EDITAR")><i class="fa fa-pencil"></i></a>&nbsp;
                <a href="javascript:;" class="btn btn-sm red tooltips btn-destroy" data-url="{{ route('secao.destroy', ['id_secao' => $secao->id_secao]) }}" data-placement="top" data-original-title="Excluir" data-message="Deseja realmente excluir esta seção e todas as suas matérias?" data-success-message="Seção excluída com sucesso!" @enabledIf("SECAO","PODE_EXCLUIR")><i class="fa fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection