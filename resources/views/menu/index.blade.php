@extends('layout')
@section('title')
<i class="fa fa-file-o"></i> Gerenciar Páginas
@endsection

@push('js_footer')
<script src="/vendor/datatables/datatables.min.js"></script>
<script src="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
@endpush

@push('style_header')
<link href="/vendor/datatables/datatables.css" rel="stylesheet" type="text/css" />
<link href="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush

@section('toolbar')
<a href="{{ route('menu.novo') }}" class="btn blue"><i class="fa fa-plus"></i> Novo Menu</a>
@endsection

@section('content')
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

<table class="datatable table table-bordered table-striped table-hover table-responsive" width="100%" data-stateSave="true" data-paging="true">
    <thead>
        <tr>
            <th width="25%">Menu</th>
            <th width="10%">Tipo</th>
            <th width="40%">Descrição</th>
            <th width="10%" class="text-center">Público</th>
            <th width="10%" class="text-center">Publicado</th>
            <th width="5%" class="text-center">Ação</th>
        </tr>
    </thead>
    <tbody>
        @foreach($menus as $menu)
        <tr>
            <td><i class="fa fa-sitemap"></i> <a href="{{ route('menu.pagina', ['id_menu' => $menu->id_menu]) }}"><strong>{{ $menu->nome }}</strong></a></td>
            <td><span class="label {{ ($menu->tipo == 'P') ? 'label-success' : 'label-default' }}">{{ ($menu->tipo == 'P') ? 'PRINCIPAL' : 'INTERNO' }}</span></td>
            <td>{{ $menu->descricao }}</td>
            <td align="center" class="text-center {{ $menu->publico == 'S' ? 'text-success' : 'text-danger' }}"><i class="fa {{ $menu->publico == 'S' ? 'fa-check' : 'fa-close' }}"></i></td>
            <td align="center" class="text-center {{ $menu->publicado == 'S' ? 'text-success' : 'text-danger' }}"><i class="fa {{ $menu->publicado == 'S' ? 'fa-check' : 'fa-close' }}"></i></td>
            <td nowrap="nowrap">
                <a href="{{ route('menu.pagina', ['id_menu' => $menu->id_menu]) }}" class="btn btn-sm blue tooltips" data-placement="top" data-original-title="Páginas"><i class="fa fa-copy"></i></a>&nbsp;
                <a href="{{ route('menu.edita', ['id_menu' => $menu->id_menu]) }}" class="btn btn-sm blue tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;
                <a href="javascript:;" class="btn btn-sm red tooltips btn-destroy" data-url="{{ route('menu.destroy', ['id_menu' => $menu->id_menu]) }}" data-placement="top" data-original-title="Excluir" data-message="Deseja realmente excluir este menu e todas as suas páginas?" data-success-message="O menu foi excluído com sucesso!"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection