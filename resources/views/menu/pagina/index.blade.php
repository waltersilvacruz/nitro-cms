@extends('layout')
@section('title')
<i class="fa fa-file-o"></i> Gerenciar Páginas
@endsection

@push('js_footer')
<script src="/components/jstree/dist/jstree.min.js"></script>
<script src="/vendor/ckeditor/ckeditor.js"></script>
<script src="/js/menu/pagina/index.min.js"></script>
@endpush

@push('style_header')
<link href="/components/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
@endpush

@section('toolbar')
<a href="{{ route('menu') }}" class="btn blue"><i class="fa fa-sitemap"></i> Ver todos os menus</a>
@endsection


@section('content')
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
<input type="hidden" name="id_menu" id="id_menu" value="{{ $id_menu }}" />
<input type="hidden" name="treeview_url" id="treeview_url" value="{{ route('menu.pagina.treeview', ['id_menu' => $id_menu]) }}" />

<div class="row">
    <div class="col-xs-4">
        <div id="treeview-container" class="scroller-xy full-height">

        </div>
    </div>
    <div class="col-xs-8">
        <div id="pagina-container" class="scroller-xy full-height" style="border-left: 1px solid #e7ecf1">
            
        </div>
    </div>
</div>
<div id="pagina-placeholder" style="display: none;">
    <center>
        <br /><br /><br />
        <img src="/img/pagina_edit.png" alt="Página" />
        <br /><br />
        <h3 class="text-muted">Clique em uma das páginas ao lado para editá-la</h3>
    </center>
</div>

@endsection