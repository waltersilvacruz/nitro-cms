<div style="width: 98%">
    <div class="row" style="padding-left: 20px;">
        <div class="col-xs-12">
            <form method="put" id="pagina-form" class="form-horizontal ajax-form" data-callback="paginaSalva" data-mark-field="true" role="form" action="{{ route('menu.pagina.update', ['id_menu' => $pagina->id_menu, 'id_pagina' => $pagina->id_pagina]) }}">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="id_menu" id="id_menu" value="{{ $pagina->id_menu }}" />
                <input type="hidden" name="id_pagina" id="id_pagina" value="{{ $pagina->id_pagina }}" />
                <input type="hidden" id="titulo" name="titulo" value="{{ $pagina->titulo }}" />
                <h4 id="pagina_titulo">{{ $pagina->titulo }}</h4>
                <hr />
                <div class="alert alert-danger form-error-display"></div>
                <div class="form-group">
                    <label class="control-label col-md-2"><strong>Tipo de Página:</strong></label>
                    <div class="col-md-10">
                        <div class="radio-list">
                            <label class="radio-inline">
                                <input type="radio" name="tipo_pagina" id="tipo_pagina1" value="1" @if($pagina->tipo_pagina == 1) checked="checked" @endif /> Página Normal</label>
                            <label class="radio-inline">
                                <input type="radio" name="tipo_pagina" id="tipo_pagina2" value="2" @if($pagina->tipo_pagina == 2) checked="checked" @endif /> Sem conteúdo </label>
                            <label class="radio-inline">
                                <input type="radio" name="tipo_pagina" id="tipo_pagina3" value="3" @if($pagina->tipo_pagina == 3) checked="checked" @endif /> Link 
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group tipo1" id="editor-field">
                    <div class="col-md-12">
                        <div id="ckeditorToolbar"></div>
                        <div id="toolbar-placeholder" style="display: none;"></div>
                        <textarea class="form-control" name="texto" id="texto">{!! $pagina->texto !!}</textarea>
                        <div id="toolbar-endline"></div>
                    </div>
                </div>

                <div class="form-group tipo1">
                    <label class="control-label col-md-2">Anexos</label>
                    <div class="col-md-10">
                        <webdisco data-type="attachment" data-input-name="anexos" data-input-value="{{ $anexos }}" data-title="Selecionar arquivos"></webdisco>
                    </div>
                </div>

                <div class="form-group tipo3">
                    <label class="control-label col-md-2"><strong>Link da página</strong></label>
                    <div class="col-md-10">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn default" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                                    <i class="fa fa-link"></i> Escolher <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="javascript:;" data-link="#"><i class="fa fa-globe"></i> Link da Web</a></li>
                                    <li class="dropdown-submenu"><a href="javascript:;"><i class="fa fa-file-text"></i> Seção de Matérias </a>
                                        <ul class="dropdown-menu">
                                        @if(count($secoes) > 0)
                                            @foreach($secoes as $secao)
                                            <li><a href="javascript:;" data-link="[secao:{{ $secao->id_secao}}/{{ $secao->slug }}">{{ $secao->nome }}</a></li>
                                            @endforeach
                                        @else
                                            <li><a href="javascript:;"><em>** nada cadastrado **</em></a></li>
                                        @endif
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu"><a href="javascript:;"><i class="fa fa-quote-right"></i> Articulista </a>
                                        <ul class="dropdown-menu">
                                        @if(count($articulistas) > 0)
                                            @foreach($articulistas as $articulista)
                                            <li><a href="javascript:;" data-link="[articulista:{{ $articulista->id_articulista}}/{{ $articulista->slug }}">{{ $articulista->nome }}</a></li>
                                            @endforeach
                                        @else
                                            <li><a href="javascript:;"><em>** nada cadastrado **</em></a></li>
                                        @endif
                                        </ul>
                                    </li>
                                    <li><a href="javascript:;" data-link="[galeria]"><i class="fa fa-picture-o"></i> Galeria de Fotos </a></li>
                                    <li><a href="javascript:;"><i class="fa fa-video-camera"></i> Categoria de Vídeos </a></li>
                                </ul>
                            </div>
                            <input type="text" class="form-control" id="redirect_input" name="redirect_input" maxlength="250" value="{{ $pagina->redirect }}" placeholder="http://">
                            <input type="hidden" id="redirect" name="redirect" value="{{ $pagina->redirect }}" />
                        </div>

                    </div> 
                </div>

                <div class="form-group tipo3">
                    <label class="control-label col-md-2"><strong>Abrir o link</strong></label>
                    <div class="col-md-3">
                        <select class="form-control" id="redirect_target" name="redirect_target">
                            <option value="" @if($pagina->redirect_target == '') selected @endif>Na mesma janela</option>
                            <option value="_blank" @if($pagina->redirect_target == '_blank') selected @endif>Em uma nova janela</option>
                        </select>
                    </div> 
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2"><strong>Publicado</strong></label>
                    <div class="col-md-10">
                        <input type="checkbox" class="form-control" id="publicado" name="publicado" value="S" @if($pagina->publicado == 'S') checked="S" @endif data-on-color="info" data-off-color="danger" data-on-text="Sim" data-off-text="Não">
                    </div> 
                </div>
                <hr />
                <div class="form-group">
                    <label class="control-label col-md-2">&nbsp;</label>
                    <div class="col-md-10">
                        <button type="button" id="btn-cancelar" class="btn red"><i class="fa fa-close"></i> Cancelar</button>&nbsp;
                        <button type="button" id="btn-salvar" class="btn green"><i class="fa fa-check"></i> Salvar página</button>
                    </div>
                </div>
            </form>
        </div>
    </div> 
</div>
