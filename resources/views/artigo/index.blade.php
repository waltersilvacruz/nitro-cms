@extends('layout')
@section('title')
<i class="fa fa fa-quote-right"></i> Artigos / <small>lista</small>
@endsection

@push('js_footer')
<script src="/vendor/datatables/datatables.min.js"></script>
<script src="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
@endpush

@push('style_header')
<link href="/vendor/datatables/datatables.css" rel="stylesheet" type="text/css" />
<link href="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush

@section('toolbar')
<a href="{{ route('artigo.novo') }}" @enabledIf("ARTIGO","PODE_INCLUIR") class="btn blue"><i class="fa fa-plus"></i> Novo Artigo</a>
@endsection

@section('content')
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

<table class="datatable table table-bordered table-striped table-hover table-responsive" 
       width="100%" 
       data-stateSave="true" 
       data-paging="true">
    <thead>
        <tr>
            <th width="10%">Data</th>
            <th width="30%">Autor</th>
            <th width="45%">Título</th>
            <th width="10%" class="text-center">Publicado</th>
            <th width="5%" class="text-center">Ação</th>
        </tr>
    </thead>
    <tbody>
        @foreach($artigos as $artigo)
        <tr>
            <td data-sort="{{ \App\Helpers\UtilHelper::getTimestamp($artigo->data_artigo) }}">{{ \App\Helpers\UtilHelper::formatDate($artigo->data_artigo) }}</td>
            <td>{{ $artigo->articulista->nome }}</td>
            <td>{{ $artigo->titulo }}</td>
            <td align="center" class="text-center {{ $artigo->publicado == 'S' ? 'text-success' : 'text-danger' }}"><i class="fa {{ $artigo->publicado == 'S' ? 'fa-check' : 'fa-close' }}"></i></td>
            <td nowrap="nowrap">
                <a href="{{ route('artigo.edita', ['id_artigo' => $artigo->id_artigo]) }}" @enabledIf("ARTIGO","PODE_EDITAR") class="btn btn-sm blue tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;
                <a href="javascript:;" class="btn btn-sm red tooltips btn-destroy" @enabledIf("ARTIGO","PODE_EXCLUIR") data-url="{{ route('artigo.destroy', ['id_artigo' => $artigo->id_artigo]) }}" data-placement="top" data-original-title="Excluir" data-message="Deseja realmente excluir este artigo?" data-success-message="Artigo excluído com sucesso!"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection