@extends('layout')
@section('title')
<i class="fa fa fa-quote-right"></i> Artigos / <small>novo artigo</small>
@endsection

@push('js_footer')
<script src="/vendor/ckeditor/ckeditor.js"></script>
<script src="/js/artigo/form.min.js"></script>
@endpush

@section('content')
<form method="post" id="artigo-form" class="ajax-form form-horizontal" data-success="Artigo incluído com sucesso!" data-redirect="{{ route('artigo') }}" data-mark-field="true" role="form" action="{{ route('artigo.create') }}">
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
    <div class="alert alert-danger form-error-display"></div>
    <div class="form-group">
        <label  class="col-sm-2 col-md-1 contro-label text-right"><strong>Autor</strong></label>
        <div class="col-sm-2">
            <select class="form-control" name="articulista" id="articulista">
                <option value=""></option>
                @foreach($articulistas as $articulista)
                <option value="{{ $articulista->id_articulista }}">{{ $articulista->nome }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-2 col-md-1 contro-label text-right"><strong>Data</strong></label>
        <div class="col-sm-1">
            <input type="text" class="form-control form-control-inline datepicker" name="data" id="data" maxlength="10" value="{{ date('d/m/Y') }}">
        </div>
        <div class="col-sm-1">
            <input type="text" class="form-control timepicker timepicker-24" name="hora" id="hora" maxlength="4" value="{{ date('H:i') }}">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2 col-md-1">Imagem</label>
        <div class="col-md-4">
            <webdisco data-type="inline_image" data-input-name="foto" data-input-value="" data-title="Escolher imagem"></webdisco>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2 col-md-1">Chapéu</label>
        <div class="col-sm-2">
            <input type="text" name="chapeu" id="chapeu" maxlength="50" class="form-control" />
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-2 col-md-1 contro-label text-right"><strong>Título</strong></label>
        <div class="col-sm-4">
            <input type="text" name="titulo" id="titulo" maxlength="100" class="form-control" />
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-2 col-md-1 contro-label text-right">Complemento</label>
        <div class="col-sm-4">
            <textarea class="form-control" name="complemento" id="complemento" rows="2"></textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2 col-md-1">Tags</label>
        <div class="col-sm-4">
            <input type="text" class="form-control tags" name="tags" id="tags" value="" />
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-2 col-md-1 contro-label text-right"><strong>Texto</strong></label>
        <div class="col-sm-8">
            <textarea class="form-control" name="texto" id="texto"></textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2 col-md-1">Anexos</label>
        <div class="col-sm-8">
            <webdisco data-type="attachment" data-input-name="anexos" data-input-value="" data-title="Selecionar arquivos"></webdisco>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2 col-md-1">Publicado</label>
        <div class="col-sm-2 col-md-1">
            <input type="checkbox" class="form-control switch" id="publicado" name="publicado" value="S" @enabledIf('artigo','publicar') checked="checked" @endacao data-on-color="info" data-off-color="danger" data-on-text="Sim" data-off-text="Não">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2 col-md-1">&nbsp;</label>
        <div class="col-sm-8">
            <a type="button" href="{{ route('artigo') }}" id="btn-cancelar" class="btn red"><i class="fa fa-close"></i> Cancelar</a>&nbsp;
            <button type="button" id="btn-salvar" class="btn green"><i class="fa fa-check"></i> Salvar matéria</button>
        </div>
    </div>
</form>
@endsection