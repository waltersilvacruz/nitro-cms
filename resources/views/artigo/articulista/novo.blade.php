@extends('layout')
@section('title')
<i class="fa fa fa-quote-right"></i> Artigos / <small>novo articulista</small>
@endsection

@section('content')
<form method="post" id="pagina-form" class="form-horizontal ajax-form" data-success="Articulista adicionado com sucesso!" data-redirect="{{ route('artigo.articulista') }}" data-mark-field="true" role="form" action="{{ route('artigo.articulista.create') }}">
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

    <div class="alert alert-danger form-error-display"></div>

    <div class="form-group">
        <label class="col-sm-2 col-md-1 contro-label text-right"><strong>Nome</strong></label>
        <div class="col-md-4">
            <input type="text" class="form-control" maxlength="50" name="nome" id="nome" placeholder="" />
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 col-md-1 contro-label text-right"><strong>Foto</strong></label>
        <div class="col-md-4">
            <webdisco data-type="inline_image" data-input-name="foto" data-input-val="" data-title="Escolher imagem"></webdisco>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 col-md-1 contro-label text-right">Resumo</label>
        <div class="col-md-7">
            <textarea class="form-control" rows="4" name="resumo" id="resumo"></textarea>
        </div> 
    </div>

    <div class="form-group">
        <label class="col-sm-2 col-md-1 contro-label text-right"><strong>Publicado</strong></label>
        <div class="col-md-5">
            <input type="checkbox" class="form-control switch" id="publicado" name="publicado" value="S" data-on-color="info" data-off-color="danger" data-on-text="Sim" data-off-text="Não">
        </div> 
    </div>



    <hr />
    <div class="form-group">
        <label class="col-sm-2 col-md-1 contro-label text-right">&nbsp;</label>
        <div class="col-md-10">
            <a href="{{ route('artigo.articulista') }}" class="btn red"><i class="fa fa-close"></i> Cancelar</a>&nbsp;
            <button type="submit" id="btn-salvar" class="btn green"><i class="fa fa-check"></i> Salvar</button>
        </div>
    </div>
</form>
@endsection