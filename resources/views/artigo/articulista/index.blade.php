@extends('layout')
@section('title')
<i class="fa fa fa-quote-right"></i> Artigos / <small>lista de autores</small>
@endsection

@push('js_footer')
<script src="/vendor/datatables/datatables.min.js"></script>
<script src="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
@endpush

@push('style_header')
<link href="/vendor/datatables/datatables.css" rel="stylesheet" type="text/css" />
<link href="/vendor/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush

@section('toolbar')
<a href="{{ route('artigo.articulista.novo') }}" @enabledIf("ARTICULISTA","PODE_INCLUIR") class="btn blue"><i class="fa fa-plus"></i> Novo Articulista</a>
@endsection

@section('content')
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

<table class="datatable table table-bordered table-striped table-hover table-responsive" 
       width="100%" 
       data-stateSave="true" 
       data-paging="true" 
       data-order='[[1, "asc"]]'>
    <thead>
        <tr>
            <th width="10%">Foto</th>
            <th width="30%">Autor</th>
            <th width="45%">Resumo</th>
            <th width="10%" class="text-center">Publicado</th>
            <th width="5%" class="text-center">Ação</th>
        </tr>
    </thead>
    <tbody>
        @foreach($articulistas as $articulista)
        <tr>
            <td><img src="{{ \App\Helpers\StorageHelper::image($articulista->foto, 100, 75) }}" class="img-responsive" border="0" /></td>
            <td>{{ $articulista->nome }}</td>
            <td>{{ $articulista->resumo }}</td>
            <td align="center" class="text-center {{ $articulista->publicado == 'S' ? 'text-success' : 'text-danger' }}"><i class="fa {{ $articulista->publicado == 'S' ? 'fa-check' : 'fa-close' }}"></i></td>
            <td nowrap="nowrap">
                <a href="{{ route('artigo.articulista.edita', ['id_articulista' => $articulista->id_articulista]) }}" @enabledIf("ARTICULISTA","PODE_EDITAR") class="btn btn-sm blue tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;
                <a href="javascript:;" class="btn btn-sm red tooltips btn-destroy" @enabledIf("ARTICULISTA","PODE_EXCLUIR") data-url="{{ route('artigo.articulista.destroy', ['id_articulista' => $articulista->id_articulista]) }}" data-placement="top" data-original-title="Excluir" data-message="Deseja realmente excluir este articulista e e todos os seus artigos?" data-success-message="Articulista excluído com sucesso!"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection