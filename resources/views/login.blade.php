<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>WebDEV NitroCMS | Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta name="robots" content="no-follow" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <link href="/components/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
        <link href="/components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="/components/bootstrap-timepicker/css/timepicker.less" rel="stylesheet/less" type="text/css" />
        <link href="/components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/login.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="icon" type="image/png" href="/favicon.png" />
    </head>
    <body class=" login">
        <div class="logo">
            <a href="index.html">
                <img src="/img/logo_big.png" alt="" />
            </a>
        </div>
        <div class="content">
            {!! Form::open(array('route' => array('autentica'))) !!}
            <h3 class="form-title text-center bold">Autenticação</h3>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Usuário</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Usuário" name="logon" id="logon" value="{{ Input::old('logon', 'admin') }}" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Senha</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Senha" name="password" value="{{ Input::old('password', 'chmod-R777') }}" /> </div>
                </div>
                <div class="form-actions padding-bottom clearfix">
                    <button type="submit" class="btn green pull-right"><i class="fa fa-lock"></i> Autenticar </button>
                </div>
                <div class="copyright padding-bottom"> 2016 &copy; WebDEV Tecnologia</div>
                {!! Form::close() !!}
        </div>
        <!--[if lt IE 9]>
        <script src="/vendor/metronic/scripts/respond.min.js"></script>
        <script src="/vendor/metronic/scripts/excanvas.min.js"></script>
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="/components/jquery/dist/jquery.min.js" type="text/javascript"></script>
        <script src="/components/underscore/underscore-min.js"></script>
        <script src="/components/bootstrap-custom/bootstrap.min.js" type="text/javascript"></script>
        <script src="/components/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="/components/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="/components/js-cookie/src/js.cookie.js" type="text/javascript"></script>
        <script src="/components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="/components/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/components/jquery.blockUI/jquery.blockUI.js" type="text/javascript"></script>
        <script src="/components/bootstrap-switch/dist/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="/components/toastr/toastr.min.js" type="text/javascript"></script>
        <script src="/components/bootstrap-timepicker/js/bootstrap-timepicker.js" type="text/javascript"></script>
        <script src="/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="/components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js" type="text/javascript"></script>
        <script src="/components/uniform/dist/js/jquery.uniform.standalone.js" type="text/javascript"></script>
        <script src="/components/backstretch/src/jquery.backstretch.js" type="text/javascript"></script>
        <script src="/components/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
        <script src="/components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script src="/components/bootstrap-3-typeahead/bootstrap3-typeahead.min.js"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="/vendor/metronic/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
<!--        <script src="/vendor/metronic/scripts/layout.min.js" type="text/javascript"></script>-->
        <script src="/vendor/metronic/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <!-- CUSTOM SCRIPTS -->
        <script src="/js/app.min.js" /></script>
        <script src="/vendor/metronic/scripts/login.min.js" type="text/javascript"></script>
    </body>
</html>
