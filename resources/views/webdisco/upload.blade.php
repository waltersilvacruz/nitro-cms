@extends('popup')
@push('js_footer')
<script src="/components/dropzone/dist/min/dropzone.min.js"></script>
<script src="/js/webdisco/upload.min.js"></script>
@endpush
@push('style_header')
<link href="/components/dropzone/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
@endpush

@section('content')
<div id="page-content" style="padding-top: 0px !important;"> 
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="id_diretorio" id="id_diretorio" value="{{ $id_diretorio }}" />
    <input type="hidden" name="upload_url" id="upload_url" value="{{ route('webdisco.files.upload.create', ['id_diretorio' => $id_diretorio]) }}" />
    <div class="dropzone" id="uploadform">
        <div class="dz-message">
            <div class="dz-custom-message">
                <div class="drag-and-drop"><i class="fa fa-cloud-upload"></i></div>
                <h1>Drag & Drop</h1>
                <p>Selecione os arquivos no seu computador e arraste para esta janela</p>
                <p>ou clique nesta área para abrir a janela de seleção dos arquivos</p>
            </div>
        </div>
    </div>
    
    
    <div id="popup-footer">
        <div>
            <button id="btn-cancelar" class="btn btn-danger btn-close-popup" type="button"><i class="fa fa-remove"></i> Cancelar</button>
            <button id="btn-submit" class="btn btn-success" type="button"><i class="fa fa-check"></i> Salvar</button>
        </div>
    </div>
</div>
@endsection
