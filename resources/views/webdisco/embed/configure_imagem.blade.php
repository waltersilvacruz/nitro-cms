@extends('webdisco.popup')
@section('title')
<i class="fa fa-folder-open"></i> Webdisco <small>/ configurar imagem</small>
@endsection

@push('js_footer')
<script src="/components/cropper/dist/cropper.min.js"></script>
<script src="/js/webdisco/configure-image.min.js"></script>
@endpush

@push('style_header')
<link href="/components/cropper/dist/cropper.min.css" rel="stylesheet" type="text/css" />
@endpush

@section('toolbar')
<div class="webdisco-toolbar-container docs-buttons">
    <div class="btn-group">
        <button type="button" class="btn btn-default tooltips" data-method="setDragMode" data-option="move">
            <i class="fa fa-arrows"></i>
        </button>
        <button type="button" class="btn btn-default tooltips" data-method="setDragMode" data-option="crop">
            <i class="fa fa-crop"></i>
        </button>
        <button type="button" class="btn btn-default tooltips" data-method="reset">
            <i class="fa fa-picture-o"></i>
        </button>
    </div>

    <div class="btn-group">
        <button type="button" class="btn btn-default" data-method="zoom" data-option="0.1">
            <i class="fa fa-search-plus"></i>
        </button>
        <button type="button" class="btn btn-default" data-method="zoom" data-option="-0.1">
            <i class="fa fa-search-minus"></i>
        </button>
    </div>    


    <div class="btn-group">
        <button type="button" class="btn btn-default" data-method="move" data-option="-10" data-second-option="0">
            <i class="fa fa-arrow-left"></i>
        </button>
        <button type="button" class="btn btn-default" data-method="move" data-option="10" data-second-option="0">
            <i class="fa fa-arrow-right"></i>
        </button>
        <button type="button" class="btn btn-default" data-method="move" data-option="0" data-second-option="-10">
            <i class="fa fa-arrow-up"></i>
        </button>
        <button type="button" class="btn btn-default" data-method="move" data-option="0" data-second-option="10">
            <i class="fa fa-arrow-down"></i>
        </button>
    </div>

    <div class="btn-group">
        <button type="button" class="btn btn-default" data-method="rotate" data-option="-90">
            <i class="fa fa-rotate-left"></i>
        </button>
        <button type="button" class="btn btn-default" data-method="rotate" data-option="90">
            <i class="fa fa-rotate-right"></i>
        </button>
    </div>

    <div class="btn-group">
        <button type="button" class="btn btn-default" data-method="scaleX" data-option="-1">
            <span class="fa fa-arrows-h"></span>
        </button>
        <button type="button" class="btn btn-default" data-method="scaleY" data-option="-1">
            <span class="fa fa-arrows-v"></span>
        </button>
    </div>
    <div class="docs-toggles" style="display: inline-table; margin-left: 4px; width: 260px; float: right;">
        <div class="btn-group btn-group-justified" data-toggle="buttons">
            <label class="btn btn-default active">
                <input type="radio" class="sr-only hidden" id="aspectRatio0" name="aspectRatio" value="1.7777777777777777">
                16:9
            </label>
            <label class="btn btn-default">
                <input type="radio" class="sr-only hidden" id="aspectRatio1" name="aspectRatio" value="1.3333333333333333">
                4:3
            </label>
            <label class="btn btn-default">
                <input type="radio" class="sr-only hidden" id="aspectRatio2" name="aspectRatio" value="1">
                1:1
            </label>
            <label class="btn btn-default">
                <input type="radio" class="sr-only hidden" id="aspectRatio3" name="aspectRatio" value="0.6666666666666666">
                2:3
            </label>
            <label class="btn btn-default">
                <input type="radio" class="sr-only hidden" id="aspectRatio4" name="aspectRatio" value="NaN">
                Livre
            </label>
        </div>
    </div>

</div>
@endsection

@section('content')
<input type="hidden" name="id_arquivo" id="id_arquivo" value="{{ $imagem->id_arquivo }}" />
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
<div class="row">
    <div class="col-xs-3" id="image-toolbar-column">
        <div id="image-toolbar-container" class="full-height">
            <div class="docs-preview clearfix">
                <div class="img-preview preview-lg"></div>
                <div class="img-preview preview-md"></div>
                <div class="img-preview preview-sm"></div>
                <div class="img-preview preview-xs"></div>
            </div>
            <form class="form" role="form">
                <div class="cropper-data visible-lg">
                    <div class="input-group">
                        <label class="input-group-addon" for="dataWidth">Largura</label>
                        <input type="text" class="form-control" id="dataWidth" placeholder="largura" disabled="disabled">
                        <span class="input-group-addon">pixels</span>
                    </div>

                    <div class="input-group">
                        <label class="input-group-addon" for="dataHeight">Altura</label>
                        <input type="text" class="form-control" id="dataHeight" placeholder="altura" disabled="disabled">
                        <span class="input-group-addon">pixels</span>
                    </div>
                </div>
            </form>

            <br />
            <form class="form" role="form">

                <div class="row">
                    <div class="col-xs-7">
                        <div class="form-group-sm">
                            <label class="control-label">Crédito</label>
                            <input type="text" class="form-control" id="credito" name="credito" maxlength="50" value="{{ $imagem->credito }}" />
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <div class="form-group-sm">
                            <label class="control-label">Posição</label>
                            <select class="form-control" size="1" id="alinhamento" name="alinhamento">
                                <option value="left" selected>Esquerda</option>
                                <option value="right">Direita</option>
                                <option value="center">Centro</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label class="control-label">Legenda</label>
                    <textarea class="form-control" rows="2" id="legenda" name="legenda">{{ $imagem->descricao }}</textarea>
                </div>

                <div class="row">
                    <div class="col col-xs-6">
                        <a href="{{ route('webdisco') }}?popup=true&filtro=imagem" class="btn btn-block default" id="btn-cancelar"><i class="fa fa-folder-open"></i> Trocar imagem</a>&nbsp;&nbsp;
                    </div>
                    <div class="col-xs-6">
                        <button class="btn btn-block green" id="btn-confirmar"><i class="fa fa-check"></i> Concluir</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <div class="col-xs-9 border-left" id="image-editor-column">

        <div id="image-editor-container" class="full-height">
            <img id="image" src="{{ $path }}" alt="Edição de imagem..." />
        </div>
    </div>
</div>
@endsection