<div id="file_info">
    @include($preview)
    
    <p class="text-sm"><strong>ID do arquivo</strong><br />#{{ $file->id_arquivo }}</p>
    <p class="text-sm"><strong>Nome original</strong><br />{{ $file->arquivo_nome }}</p>
    <p class="text-sm"><strong>Título</strong><br />{{ $file->titulo }}</p>
    @if($file->descricao)
    <p class="text-sm"><strong>Descrição</strong><br />{{ $file->descricao }}</p>
    @endif
    @if($file->credito)
    <p class="text-sm"><strong>Crédito</strong><br />{{ $file->credito }}</p>
    @endif
    <p class="text-sm"><strong>Tamanho</strong><br />{{ App\Helpers\UtilHelper::filesize($file->arquivo_tamanho) }}</p> 
    @if($file->total_acesso > 0)
    <p class="text-sm"><strong>Acessos</strong><br />{{ $file->total_acessos }}</p> 
    @endif
    <p class="text-sm"><strong>Criado por</strong><br />{{ $file->created_by }}</p> 
    <p class="text-sm"><strong>Criado em</strong><br />{{ App\Helpers\UtilHelper::formatDate($file->created_at, true) }}</p> 
    @if($file->updated_at)
    <p class="text-sm"><strong>Atualizado em</strong><br />{{ App\Helpers\UtilHelper::formatDate($file->updated_at, true) }}</p> 
    @endif    
    
    <p>
        <div class="row">
            
            <div @if($popup == 'N') class="col-xs-6" @else class="col-xs-12" @endif>
                <a class="btn btn-default btn-block" href="{{ route('webdisco.file.download', ['id_arquivo' => $file->id_arquivo]) }}" target="_blank"><i class="fa fa-download"></i> <strong>download</strong></a>
            </div>
            @if($popup == 'N')
            <div class="col-xs-6">
                <button type="button" id="btn-editar" class="btn green btn-block"><i class="fa fa-pencil"></i> Editar</button>
            </div>
            @endif

        </div>
    </p>    
</div>

<div id="file_edit" style="display: none;">
    <form role="form" class="form">
    
        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="id_diretorio" id="id_diretorio" value="{{ $file->id_diretorio }}" />
        <input type="hidden" name="upload_url" id="upload_url" value="{{ route('webdisco.files.upload.update', ['id_diretorio' => $file->id_diretorio, 'id_arquivo' => $file->id_arquivo]) }}" />
        <div class="dropzone dz-small" id="uploadform">
            <div class="dz-message">
                <div class="dz-custom-message">
                    <div class="drag-and-drop"><i class="fa fa-cloud-upload"></i></div>
                    <p>Arraste e solte o arquivo aqui!<br /><strong>(Apenas se desejar trocá-lo)</strong></p>
                </div>
            </div>
        </div>
        <br />
        <div class="form-group">
            <label><strong>Título</strong></label>
            <input type="text" class="form-control" name="titulo" id="titulo" maxlength="100" value="{{ $file->titulo }} " />
        </div>


        <div class="form-group">
            <label><strong>Descrição</strong></label>
            <textarea class="form-control" rows="3" name="descricao" id="descricao">{{ $file->descricao }}</textarea>
        </div>

        <div class="form-group">
            <label><strong>Crédito</strong></label>
            <input type="text" class="form-control" maxlength="50" name="credito" id="credito" value="{{ $file->credito }} " />
        </div>

        <p>
        <div class="row">
            <div class="col-xs-6">
                <button type="button" id="btn-editar-cancela" class="btn red btn-block"><i class="fa fa-close"></i> Cancelar</button>
            </div>
            <div class="col-xs-6">
                <button type="button" id="btn-editar-salva" class="btn green btn-block"><i class="fa fa-check"></i> Salvar</button>
            </div>

        </div>
    </p>
        
    </form>
</div>



<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
