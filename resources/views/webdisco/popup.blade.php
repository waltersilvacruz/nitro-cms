<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Webadmin</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <link href="/components/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
        <link href="/components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="/components/bootstrap-timepicker/css/timepicker.less" rel="stylesheet/less" type="text/css" />
        <link href="/components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="/css/popup.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="icon" type="image/png" href="/favicon.png" />
        @stack('style_header')        
    </head>
    <!-- END HEAD -->

    <body class="page-content-white">
        <div class="page-content-wrapper">
            <div class="main-content" style="margin: 25px;">
                <div class="page-bar">
                    <!-- @include('layout.breadcrumb')-->
                    <span class="pull-right">
                        <p>
                            @yield('toolbar')
                        </p>
                    </span>
                </div>
                @yield('content')
            </div>
            <!-- MODAL -->
            @include('layout.modal')
            <!-- END MODAL -->        
        </div>

        <!--[if lt IE 9]>
        <script src="/vendor/metronic/scripts/respond.min.js"></script>
        <script src="/vendor/metronic/scripts/excanvas.min.js"></script>
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="/components/jquery/dist/jquery.min.js" type="text/javascript"></script>
        <script src="/components/underscore/underscore-min.js"></script>
        <script src="/components/bootstrap-custom/bootstrap.min.js" type="text/javascript"></script>
        <script src="/components/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="/components/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="/components/js-cookie/src/js.cookie.js" type="text/javascript"></script>
        <script src="/components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="/components/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/components/jquery.blockUI/jquery.blockUI.js" type="text/javascript"></script>
        <script src="/components/bootstrap-switch/dist/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="/components/toastr/toastr.min.js" type="text/javascript"></script>
        <script src="/components/bootstrap-timepicker/js/bootstrap-timepicker.js" type="text/javascript"></script>
        <script src="/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="/components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js" type="text/javascript"></script>
        <script src="/components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script src="/components/bootstrap-3-typeahead/bootstrap3-typeahead.min.js"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="/vendor/metronic/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="/js/layout.min.js" type="text/javascript"></script>
        <script src="/vendor/metronic/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <!-- CUSTOM SCRIPTS -->
        <script src="/js/app.min.js" /></script>
        @stack('js_footer')
        @include('layout.script-templates')
</body>

</html>