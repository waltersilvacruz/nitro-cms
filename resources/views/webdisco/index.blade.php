@extends($layout)
@section('title')
<i class="fa fa-folder-open"></i> Webdisco
@endsection

@push('js_footer')
<script src="/components/jstree/dist/jstree.min.js"></script>
<script src="/components/dropzone/dist/min/dropzone.min.js"></script>
<script src="/js/webdisco/index.min.js"></script>
@endpush

@push('style_header')
<link href="/components/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
<link href="/components/dropzone/dist/min/dropzone.min.css" rel="stylesheet" type="text/css" />
@endpush


@section('toolbar')
<div class="webdisco-toolbar-container">
<input type="text" class="form-control" name="keyword" style="display: none; max-width: 200px;" id="keyword" placeholder="Procurar no webdisco..." />
<button class="btn default" id="btn-start-search" ><i class="fa fa-search"></i></button>
<button class="btn red" id="btn-end-search" style="display: none;"><i class="fa fa-close"></i></button>
&nbsp;&nbsp;
<button class="btn green" id="btn-seleciona-item-popup" disabled="disabled" style="display: none;"><i class="fa fa-check"></i> Selecionar</button>
<button class="btn default" id="btn-upload" disabled="disabled"><i class="fa fa-upload"></i> Upload</button>
<div class="btn-group" id="btn-group-selecao"  style="display: none;">
    <button class="btn default dropdown-toggle" disabled="disabled" id="btn-selecao" href="javascript:;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-check"></i> Selecionar <span class="caret"></span></button>
    <ul class="dropdown-menu">
        <li><a href="javascript:;" id="btn-selecao-tudo"><i class="fa fa-check-square-o"></i> Selecionar tudo</a></li>
        <li><a href="javascript:;" id="btn-selecao-desmarcar"><i class="fa fa-times"></i> Desmarcar tudo</a></li>
    </ul>
</div>
<div class="btn-group">
    <button class="btn default dropdown-toggle" id="btn-exibir" href="javascript:;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-eye"></i> Exibir <span class="caret"></span></button>
    <ul class="dropdown-menu">
        <li><a href="javascript:;" class="btn-viewmode" data-viewmode="1"><i class="fa fa-image"></i> Ícones grandes</a></li>
        <li><a href="javascript:;" class="btn-viewmode" data-viewmode="2"><i class="fa fa-th-list"></i> Detalhado</a></li>
    </ul>
</div>
<button class="btn default" disabled="disabled" id="btn-apagar"><i class="fa fa-trash"></i> Excluir</button>
</div>
@endsection

@section('content')
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
<input type="hidden" name="popup" id="popup" value="{{ $popup }}" />
<input type="hidden" name="filtro" id="tipo" value="{{ $filtro }}" />
<input type="hidden" name="filtro" id="filtro" value="{{ $filtro }}" />
<input type="hidden" name="target" id="target" value="{{ $target }}" />

<div class="row webdisco">
    <div class="col-xs-5" id="webdisco-folders-container">
        <div id="treeview-container" class="scroller-xy treeview full-height" data-url="{{ route('webdisco.treeview') }}"></div>
    </div>
    <div class="col-xs-7 border-left" id="webdisco-files-container">
        <div id="files_container" class="scroller-xy full-height">
                    
        </div>
    </div>
</div>

<div id="search-placeholder" style="display: none;">
    <center>
        <br /><br /><br />
        <img src="/img/webdisco_search.png" alt="Pesquisa" />
        <br /><br />
        <h3 class="text-muted">Digite o texto a ser pesquisado e tecle "ENTER"</h3>
    </center>
</div>

<div id="empty-placeholder" style="display: none;">
    <center>
        <br /><br /><br />
        <img src="/img/webdisco_empty.png" alt="Página" />
        <br /><br />
        <h3 class="text-muted">Nenhum arquivo armazenado nessa pasta.</h3>
    </center>
</div>

<div id="default-placeholder" style="display: none;">
    <center>
        <br /><br /><br />
        <img src="/img/webdisco_empty.png" alt="Página" />
        <br /><br />
        <h3 class="text-muted">Selecione uma das pastas ao lado para listar os arquivos.</h3>
    </center>
</div>


<script type='text/template' id='files_preview1'>
    <% _.each(data.items, function(item){ %>
        <div class="webdisco-file col col-md-2 col-sm-2 col-xs-1 text-center">
            <a href="#" class="webdisco-file-item webdisco-file-item-embed sidebar" data-id="<%- item.id_arquivo %>" data-folder-id="<%- item.id_diretorio %>" data-folder-path="<%- data.info.path %>">
                <div class="file-icon-container" data-thumbnail="<%- item.thumbnail %>">
                    <% if(item.arquivo_extensao === 'jpg' || item.arquivo_extensao === 'jpeg') { %>
                     <div class="webdisco-thumbnail" style="background:url(<%- item.thumbnail %>) no-repeat center center;"></div>
                    <% } else { %>
                    <div class="file-icon-background" data-extension="<%- item.arquivo_extensao %>">
                        <div class="file-icon-extension"><%- item.arquivo_extensao %></div>
                    </div>
                    <% } %>
                </div>
                <div class="file-icon-name"><%- item.titulo %></div>
            </a>
        </div>
    <% }) %>
</script>

<script type='text/template' id='files_preview2'>
    <table class="table table-hover table-light">
        <tbody>
            <% _.each(data.items, function(item){ %>
            <tr class="webdisco-file-item sidebar" data-id="<%- item.id_arquivo %>" data-folder-id="<%- item.id_diretorio %>" data-folder-path="<%- data.info.path %>">
                <td width="64">
                    <a href="#" class="webdisco-file-item webdisco-file-item-block sidebar" data-id="<%- item.id_arquivo %>" data-folder-id="<%- item.id_diretorio %>" data-folder-path="<%- data.info.path %>">
                <% if(item.arquivo_extensao === 'jpg' || item.arquivo_extensao === 'jpeg') { %>
                     <div class="webdisco-thumbnail  webdisco-thumbnail-small" style="min-width: 64px; background:url(<%- item.thumbnail_small %>) no-repeat center center;"></div>
                <% } else { %>
                    <div class="file-icon-background-small" data-extension="<%- item.arquivo_extensao %>">
                        <div class="file-icon-extension"><%- item.arquivo_extensao %></div>
                    </div>
                <% } %>
                     </a>
                </td>
                <td width="55%"><a href="#" class="webdisco-file-item webdisco-file-item-embed webdisco-file-item-block sidebar" data-id="<%- item.id_arquivo %>" data-folder-id="<%- item.id_diretorio %>" data-folder-path="<%- data.info.path %>"><%- item.titulo %></a></td>
                <td width="5%" nowrap>ID #<%- item.id_arquivo %></td>
                <td width="10%"><%- item.created_by %></td>
                <td width="10%"><%- item.arquivo_tamanho %></td>
                <td width="20%"><%- item.data_formatada %></td>
            </tr>
            <% }) %>
        </tbody>
    </thead>
</script>

@endsection