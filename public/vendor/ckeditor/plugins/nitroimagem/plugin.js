CKEDITOR.plugins.add('nitroimagem', {
    icons: 'nitroimagem',
    init: function (editor) {
        editor.addCommand('nitroimagem', {
            exec: function(editor) {
                Nitro.webdisco({
                    filtro: 'imagem'
                });
            }
        });
        editor.ui.addButton('NitroImagem', {
            label: 'Inserir imagem',
            command: 'nitroimagem',
            toolbar: 'insert,0'
        });
    }
});