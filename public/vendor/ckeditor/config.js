/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    config.allowedContent = true;
    config.language = 'pt-br';
    config.contentsCss = '/css/ckeditor-custom.min.css?ts=' + Date.now();
    config.extraPlugins = 'autogrow,sharedspace,nitroimagem';
    config.height = 380;
    config.autoGrow_minHeight = 380;
    config.autoGrow_bottomSpace = 50;
    config.sharedSpaces = {
        top: 'ckeditorToolbar'
    };
    config.toolbarGroups = [
        {name: 'document', groups: ['mode']},
        {name: 'clipboard', groups: ['clipboard', 'undo']},
        {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
        {name: 'forms', groups: ['forms']},
        {name: 'insert', groups: ['insert']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
        '/',
        {name: 'styles', groups: ['styles']},
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'colors', groups: ['colors']},
        {name: 'links', groups: ['links']},
        {name: 'tools', groups: ['tools']},
        {name: 'others', groups: ['others']},
        {name: 'about', groups: ['about']}
    ];

    config.removeButtons = 'Save,NewPage,Preview,Print,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Language,BidiRtl,BidiLtr,Flash,Smiley,PageBreak,Iframe,About,Image,Font,FontSize';
};
