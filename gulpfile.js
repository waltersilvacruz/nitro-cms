var gulp = require('gulp-help')(require('gulp'));
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var del = require('del');
var chmod = require('gulp-chmod');
var composer = require('gulp-composer');
var shell = require('gulp-shell');
var copy = new(require('task-copy'));

gulp.task('clean:js', 'Remove os Javascripts compilados para distribuição', function () {
    return del([
        'public/js/**/*.js'
    ]);
});

gulp.task('clean:css', 'Remove os CSSs compilados para distribuição', function () {
    return del([
        'public/css/**/*.css'
    ]);
});

gulp.task('clean:assets', 'Remove os Javascripts e CSSs compilados para distribuição', function () {
    return gulp.start('clean:js')
            .start('clean:css');
});

gulp.task('compile:css', 'Compila os arquivos SCSS', function () {
    gulp.src('./resources/assets/sass/ckeditor-custom.min.scss')
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(chmod(664))
            .pipe(gulp.dest('./public/css'));

    gulp.src('./resources/assets/sass/app.min.scss')
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(chmod(664))
            .pipe(gulp.dest('./public/css'));

    gulp.src('./resources/assets/sass/login.min.scss')
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(chmod(664))
            .pipe(gulp.dest('./public/css'));

    return gulp.src('./resources/assets/sass/popup.min.scss')
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(chmod(664))
            .pipe(gulp.dest('./public/css'));

});

gulp.task('compile:js', 'Compila os arquivos Javascripts', function (done) {
    gulp.src('./resources/assets/js/**/*.js')
        .pipe(uglify())
        .on('error', onJsError)
        .on('end', done)
        .pipe(chmod(664))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('compile', 'Compila os arquivos SCSS e Javascripts e copia todas as dependências', function () {
    return gulp.start('compile:js')
            .start('compile:css');
});

gulp.task('autoload', 'Recompila o autoload do composer', function () {
    return(composer('dumpautoload', {optimize: true}));
});

gulp.task('vendor:assets', 'Copia os assets de terceiros para a aplicação', function () {
    copy.run('./node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', {
        dest: './public/components/bootstrap-custom/bootstrap.min.js',
        recursive: true,
        backup: false,
        verbose: false
    });

    copy.run([
        './node_modules/bootstrap-sass/assets/fonts/bootstrap/glyphicons-halflings-regular.eot',
        './node_modules/bootstrap-sass/assets/fonts/bootstrap/glyphicons-halflings-regular.svg',
        './node_modules/bootstrap-sass/assets/fonts/bootstrap/glyphicons-halflings-regular.ttf',
        './node_modules/bootstrap-sass/assets/fonts/bootstrap/glyphicons-halflings-regular.woff',
        './node_modules/bootstrap-sass/assets/fonts/bootstrap/glyphicons-halflings-regular.woff2'
        ], {
        dest: './public/fonts/bootstrap',
        recursive: false,
        backup: false,
        verbose: false
    });

    copy.run([
        './node_modules/font-awesome/fonts/fontawesome-webfont.eot',
        './node_modules/font-awesome/fonts/fontawesome-webfont.svg',
        './node_modules/font-awesome/fonts/fontawesome-webfont.ttf',
        './node_modules/font-awesome/fonts/fontawesome-webfont.woff',
        './node_modules/font-awesome/fonts/fontawesome-webfont.woff2',
        './node_modules/font-awesome/fonts/FontAwesome.otf'
        ], {
        dest: './public/fonts',
        recursive: false,
        backup: false,
        verbose: false
    });
});

gulp.task('watch', 'Monitora as alterações nos arquivos SCSS e Javascripts', function () {
    gulp.start('autoload');
    gulp.start('vendor:assets');
    gulp.start('clean:assets');
    gulp.start('compile');

    watch('./resources/assets/sass/**/*.scss', function () {
        gulp.start('compile:css');
    });

    watch('./resources/assets/js/**/*.js', function () {
        gulp.start('compile:js');
    });
});

gulp.task('default', 'Compila a aplicação para deploy', function () {
    gulp.start('autoload');
    gulp.start('vendor:assets');
    gulp.start('clean:assets');
    gulp.start('compile');
});


var onJsError = function(err) {
    console.log(err);
    this.emit('end');
};
