-- MySQL dump 10.13  Distrib 5.5.44, for osx10.10 (x86_64)
--
-- Host: localhost    Database: nitrocms
-- ------------------------------------------------------
-- Server version	5.5.44

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `arquivo`
--

DROP TABLE IF EXISTS `arquivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arquivo` (
  `id_arquivo` int(11) NOT NULL AUTO_INCREMENT,
  `id_diretorio` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descricao` longtext,
  `credito` varchar(50) DEFAULT NULL,
  `arquivo_nome` varchar(300) NOT NULL,
  `arquivo_tamanho` bigint(20) NOT NULL,
  `arquivo_mime_type` varchar(100) NOT NULL,
  `arquivo_extensao` varchar(10) NOT NULL,
  `arquivo_hash` varchar(300) NOT NULL,
  `publicado` char(1) NOT NULL DEFAULT 'S',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_arquivo`),
  KEY `fk_arquivo_diretorio1_idx` (`id_diretorio`),
  KEY `fk_arquivo_usuario1_idx` (`created_by`),
  CONSTRAINT `fk_arquivo_diretorio1` FOREIGN KEY (`id_diretorio`) REFERENCES `diretorio` (`id_diretorio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_arquivo_usuario1` FOREIGN KEY (`created_by`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arquivo`
--

LOCK TABLES `arquivo` WRITE;
/*!40000 ALTER TABLE `arquivo` DISABLE KEYS */;
INSERT INTO `arquivo` (`id_arquivo`, `id_diretorio`, `titulo`, `descricao`, `credito`, `arquivo_nome`, `arquivo_tamanho`, `arquivo_mime_type`, `arquivo_extensao`, `arquivo_hash`, `publicado`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES (1,3,'1469779631_space-rocket',NULL,NULL,'1469779631_space-rocket.png',23691,'image/png','png','1469779631-space-rocket.png','S',1,'2016-07-29 00:52:34','2016-07-29 00:52:34',NULL);
/*!40000 ALTER TABLE `arquivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controle_acesso`
--

DROP TABLE IF EXISTS `controle_acesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controle_acesso` (
  `conteudo` int(11) NOT NULL,
  `tipo` char(1) NOT NULL,
  `total_acesso` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `controle_acessocol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`conteudo`,`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controle_acesso`
--

LOCK TABLES `controle_acesso` WRITE;
/*!40000 ALTER TABLE `controle_acesso` DISABLE KEYS */;
/*!40000 ALTER TABLE `controle_acesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diretorio`
--

DROP TABLE IF EXISTS `diretorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diretorio` (
  `id_diretorio` int(11) NOT NULL AUTO_INCREMENT,
  `id_diretorio_superior` varchar(45) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `publicado` char(1) NOT NULL DEFAULT 'S',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_diretorio`),
  KEY `fk_diretorio_usuario1_idx` (`created_by`),
  CONSTRAINT `fk_diretorio_usuario1` FOREIGN KEY (`created_by`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diretorio`
--

LOCK TABLES `diretorio` WRITE;
/*!40000 ALTER TABLE `diretorio` DISABLE KEYS */;
INSERT INTO `diretorio` (`id_diretorio`, `id_diretorio_superior`, `nome`, `publicado`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES (1,'0','Imagens','Y',1,'2016-07-29 00:23:27','2016-07-29 00:23:27',NULL),(2,'0','Vídeos','Y',1,'2016-07-29 00:23:47','2016-07-29 00:23:47',NULL),(3,'0','Documentos','Y',1,'2016-07-29 00:23:51','2016-07-29 00:23:51',NULL),(4,'3','OpenOffice','S',1,'2016-07-29 00:45:53','2016-07-29 00:46:37','2016-07-29 00:46:37');
/*!40000 ALTER TABLE `diretorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `owner_type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `owner_id` int(11) NOT NULL,
  `old_value` text CHARACTER SET utf8,
  `new_value` text CHARACTER SET utf8,
  `type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `route` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ip` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` (`id`, `user_id`, `owner_type`, `owner_id`, `old_value`, `new_value`, `type`, `route`, `ip`, `created_at`, `updated_at`) VALUES (1,1,'App\\Databases\\Models\\DiretorioModel',4,'null','{\"id_diretorio_superior\":\"3\",\"nome\":\"Nova Pasta\",\"publicado\":\"S\",\"created_by\":1}','created','webdisco.treeview.create','::1','2016-07-29 04:45:53','2016-07-29 04:45:53'),(2,1,'App\\Databases\\Models\\DiretorioModel',4,'{\"nome\":\"Nova Pasta\"}','{\"nome\":\"PDF\"}','updated','webdisco.treeview.update','::1','2016-07-29 04:45:57','2016-07-29 04:45:57'),(3,1,'App\\Databases\\Models\\DiretorioModel',4,'{\"nome\":\"PDF\"}','{\"nome\":\"OpenOffice\"}','updated','webdisco.treeview.update','::1','2016-07-29 04:46:31','2016-07-29 04:46:31'),(4,1,'App\\Databases\\Models\\DiretorioModel',4,'{\"id_diretorio\":4,\"id_diretorio_superior\":\"3\",\"nome\":\"OpenOffice\",\"publicado\":\"S\",\"created_by\":1,\"created_at\":\"2016-07-29 00:45:53\",\"updated_at\":\"2016-07-29 00:46:31\",\"deleted_at\":{\"date\":\"2016-07-29 00:46:37.000000\",\"timezone_type\":3,\"timezone\":\"America\\/Cuiaba\"}}','null','deleted','webdisco.treeview.destroy','::1','2016-07-29 04:46:37','2016-07-29 04:46:37'),(5,1,'App\\Databases\\Models\\ArquivoModel',1,'null','{\"id_diretorio\":\"3\",\"titulo\":\"1469779631_space-rocket\",\"descricao\":null,\"credito\":null,\"arquivo_nome\":\"1469779631_space-rocket.png\",\"arquivo_extensao\":\"png\",\"arquivo_tamanho\":23691,\"arquivo_mime_type\":\"image\\/png\",\"arquivo_hash\":\"1469779631-space-rocket.png\",\"publicado\":\"S\",\"created_by\":1}','created','webdisco.files.upload.create','::1','2016-07-29 04:52:34','2016-07-29 04:52:34');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materia`
--

DROP TABLE IF EXISTS `materia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materia` (
  `id_materia` int(11) NOT NULL AUTO_INCREMENT,
  `id_secao` int(11) NOT NULL,
  `data_materia` datetime NOT NULL,
  `autor_nome` varchar(50) DEFAULT NULL,
  `autor_email` varchar(50) DEFAULT NULL,
  `autor_local` varchar(50) DEFAULT NULL,
  `chapeu` varchar(50) DEFAULT NULL,
  `titulo` varchar(200) NOT NULL,
  `slug` varchar(210) NOT NULL,
  `complemento` varchar(500) DEFAULT NULL,
  `texto` longtext NOT NULL,
  `publicado` char(1) NOT NULL DEFAULT 'N',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_materia`),
  KEY `fk_materia_secao1_idx` (`id_secao`),
  KEY `fk_materia_usuario1_idx` (`created_by`),
  CONSTRAINT `fk_materia_secao1` FOREIGN KEY (`id_secao`) REFERENCES `secao` (`id_secao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_materia_usuario1` FOREIGN KEY (`created_by`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materia`
--

LOCK TABLES `materia` WRITE;
/*!40000 ALTER TABLE `materia` DISABLE KEYS */;
/*!40000 ALTER TABLE `materia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materia_anexo`
--

DROP TABLE IF EXISTS `materia_anexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materia_anexo` (
  `id_materia_anexo` int(11) NOT NULL AUTO_INCREMENT,
  `id_materia` int(11) NOT NULL,
  `id_arquivo` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_materia_anexo`),
  KEY `fk_materia_anexo_materia1_idx` (`id_materia`),
  KEY `fk_materia_anexo_arquivo1_idx` (`id_arquivo`),
  KEY `fk_materia_anexo_usuario1_idx` (`created_by`),
  CONSTRAINT `fk_materia_anexo_materia1` FOREIGN KEY (`id_materia`) REFERENCES `materia` (`id_materia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_materia_anexo_arquivo1` FOREIGN KEY (`id_arquivo`) REFERENCES `arquivo` (`id_arquivo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_materia_anexo_usuario1` FOREIGN KEY (`created_by`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materia_anexo`
--

LOCK TABLES `materia_anexo` WRITE;
/*!40000 ALTER TABLE `materia_anexo` DISABLE KEYS */;
/*!40000 ALTER TABLE `materia_anexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` char(1) NOT NULL DEFAULT 'I' COMMENT 'I - Interno / P - Principal',
  `nome` varchar(100) NOT NULL,
  `descricao` longtext,
  `publicado` char(1) NOT NULL DEFAULT 'N',
  `publico` char(1) NOT NULL DEFAULT 'S',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_menu`),
  KEY `fk_menu_usuario_idx` (`created_by`),
  CONSTRAINT `fk_menu_usuario` FOREIGN KEY (`created_by`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagina`
--

DROP TABLE IF EXISTS `pagina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagina` (
  `id_pagina` int(11) NOT NULL AUTO_INCREMENT,
  `id_pagina_superior` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `tipo_pagina` char(1) NOT NULL DEFAULT '1' COMMENT '1 - Normal / 2 - Sem conteudo / 3 - Link',
  `titulo` varchar(200) NOT NULL,
  `slug` varchar(210) NOT NULL,
  `redirect` varchar(250) DEFAULT NULL,
  `redirect_target` varchar(10) DEFAULT NULL,
  `texto` longtext,
  `publicado` char(1) NOT NULL DEFAULT 'N',
  `ordem` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pagina`),
  KEY `fk_pagina_menu1_idx` (`id_menu`),
  KEY `fk_pagina_usuario1_idx` (`created_by`),
  CONSTRAINT `fk_pagina_menu1` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pagina_usuario1` FOREIGN KEY (`created_by`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagina`
--

LOCK TABLES `pagina` WRITE;
/*!40000 ALTER TABLE `pagina` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagina_anexo`
--

DROP TABLE IF EXISTS `pagina_anexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagina_anexo` (
  `id_pagina_anexo` int(11) NOT NULL AUTO_INCREMENT,
  `id_pagina` int(11) NOT NULL,
  `id_arquivo` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pagina_anexo`),
  KEY `fk_pagina_anexo_pagina1_idx` (`id_pagina`),
  KEY `fk_pagina_anexo_arquivo1_idx` (`id_arquivo`),
  KEY `fk_pagina_anexo_usuario1_idx` (`created_by`),
  CONSTRAINT `fk_pagina_anexo_pagina1` FOREIGN KEY (`id_pagina`) REFERENCES `pagina` (`id_pagina`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pagina_anexo_arquivo1` FOREIGN KEY (`id_arquivo`) REFERENCES `arquivo` (`id_arquivo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pagina_anexo_usuario1` FOREIGN KEY (`created_by`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagina_anexo`
--

LOCK TABLES `pagina_anexo` WRITE;
/*!40000 ALTER TABLE `pagina_anexo` DISABLE KEYS */;
/*!40000 ALTER TABLE `pagina_anexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secao`
--

DROP TABLE IF EXISTS `secao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secao` (
  `id_secao` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` longtext,
  `publico` char(1) NOT NULL DEFAULT 'S',
  `publicado` char(1) NOT NULL DEFAULT 'N',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_secao`),
  KEY `fk_secao_usuario1_idx` (`created_by`),
  CONSTRAINT `fk_secao_usuario1` FOREIGN KEY (`created_by`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secao`
--

LOCK TABLES `secao` WRITE;
/*!40000 ALTER TABLE `secao` DISABLE KEYS */;
/*!40000 ALTER TABLE `secao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `tag` varchar(30) NOT NULL,
  `slug` varchar(35) NOT NULL,
  PRIMARY KEY (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_conteudo`
--

DROP TABLE IF EXISTS `tag_conteudo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_conteudo` (
  `tag` varchar(30) NOT NULL,
  `conteudo` int(11) NOT NULL,
  `tipo` char(1) NOT NULL,
  PRIMARY KEY (`tag`,`tipo`,`conteudo`),
  KEY `fk_tag_conteudo_tag1_idx` (`tag`),
  CONSTRAINT `fk_tag_conteudo_tag1` FOREIGN KEY (`tag`) REFERENCES `tag` (`tag`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_conteudo`
--

LOCK TABLES `tag_conteudo` WRITE;
/*!40000 ALTER TABLE `tag_conteudo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag_conteudo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `logon` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `validade` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id_usuario`, `nome`, `email`, `logon`, `password`, `remember_token`, `validade`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES (1,'Administrador','walter@tnx.com.br','admin','$2y$10$UTFx5c4/5W7Lw5Y7rybV7eHIrW9RRSm7wfGzlgJsGcfwPvHxLInNu',NULL,NULL,1,'2016-07-28 21:37:23','2016-07-28 21:37:23',NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vw_arquivo`
--

DROP TABLE IF EXISTS `vw_arquivo`;
/*!50001 DROP VIEW IF EXISTS `vw_arquivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_arquivo` (
  `id_arquivo` tinyint NOT NULL,
  `id_diretorio` tinyint NOT NULL,
  `titulo` tinyint NOT NULL,
  `descricao` tinyint NOT NULL,
  `credito` tinyint NOT NULL,
  `arquivo_nome` tinyint NOT NULL,
  `arquivo_tamanho` tinyint NOT NULL,
  `arquivo_mime_type` tinyint NOT NULL,
  `arquivo_extensao` tinyint NOT NULL,
  `arquivo_hash` tinyint NOT NULL,
  `publicado` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL,
  `deleted_at` tinyint NOT NULL,
  `total_acesso` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_pagina`
--

DROP TABLE IF EXISTS `vw_pagina`;
/*!50001 DROP VIEW IF EXISTS `vw_pagina`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_pagina` (
  `id_pagina` tinyint NOT NULL,
  `id_pagina_superior` tinyint NOT NULL,
  `id_menu` tinyint NOT NULL,
  `tipo_pagina` tinyint NOT NULL,
  `titulo` tinyint NOT NULL,
  `slug` tinyint NOT NULL,
  `redirect` tinyint NOT NULL,
  `redirect_target` tinyint NOT NULL,
  `texto` tinyint NOT NULL,
  `publicado` tinyint NOT NULL,
  `ordem` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL,
  `deleted_at` tinyint NOT NULL,
  `tem_link` tinyint NOT NULL,
  `tem_conteudo` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'nitrocms'
--

--
-- Final view structure for view `vw_arquivo`
--

/*!50001 DROP TABLE IF EXISTS `vw_arquivo`*/;
/*!50001 DROP VIEW IF EXISTS `vw_arquivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_arquivo` AS select `a`.`id_arquivo` AS `id_arquivo`,`a`.`id_diretorio` AS `id_diretorio`,`a`.`titulo` AS `titulo`,`a`.`descricao` AS `descricao`,`a`.`credito` AS `credito`,`a`.`arquivo_nome` AS `arquivo_nome`,`a`.`arquivo_tamanho` AS `arquivo_tamanho`,`a`.`arquivo_mime_type` AS `arquivo_mime_type`,`a`.`arquivo_extensao` AS `arquivo_extensao`,`a`.`arquivo_hash` AS `arquivo_hash`,`a`.`publicado` AS `publicado`,`a`.`created_by` AS `created_by`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_at` AS `deleted_at`,`ca`.`total_acesso` AS `total_acesso` from (`arquivo` `a` left join `controle_acesso` `ca` on(((`ca`.`conteudo` = `a`.`id_arquivo`) and (`ca`.`tipo` = 'A')))) where isnull(`a`.`deleted_at`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_pagina`
--

/*!50001 DROP TABLE IF EXISTS `vw_pagina`*/;
/*!50001 DROP VIEW IF EXISTS `vw_pagina`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_pagina` AS select `p`.`id_pagina` AS `id_pagina`,`p`.`id_pagina_superior` AS `id_pagina_superior`,`p`.`id_menu` AS `id_menu`,`p`.`tipo_pagina` AS `tipo_pagina`,`p`.`titulo` AS `titulo`,`p`.`slug` AS `slug`,`p`.`redirect` AS `redirect`,`p`.`redirect_target` AS `redirect_target`,`p`.`texto` AS `texto`,`p`.`publicado` AS `publicado`,`p`.`ordem` AS `ordem`,`p`.`created_by` AS `created_by`,`p`.`created_at` AS `created_at`,`p`.`updated_at` AS `updated_at`,`p`.`deleted_at` AS `deleted_at`,(case when (`p`.`redirect` is not null) then 'S' when isnull(`p`.`redirect`) then 'N' end) AS `tem_link`,(case when (`p`.`texto` is not null) then 'S' when isnull(`p`.`texto`) then 'N' end) AS `tem_conteudo` from `pagina` `p` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-29  1:02:43
