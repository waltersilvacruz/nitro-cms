# WebDEV - Nitro Content Management System

## Instalação

```
composer install
npm install
bower install
gulp
cp .env.example .env
php artisan key:generate
ln -s storage/app public/storage 
```
