<?php

namespace App\Helpers;

use App\Helpers\UtilHelper;

class StorageHelper {

    public static function filePath($file, $relative = true) {
        if (!$file) {
            return('');
        }
        $data = UtilHelper::formatDate($file->created_at);
        list($dia, $mes, $ano) = explode('/', $data);
        
        $path = $relative ?  env('WEBDISCO_BASE_URL') : env('WEBDISCO_BASE_PATH');
        
        if($file->arquivo_extensao === 'jpg' || $file->arquivo_extensao === 'jpeg') {
            $path .= '/' . $ano . '/' . $mes . '/jpg/original/' . $file->arquivo_hash;
        } else {
            $path .=  '/' . $ano . '/' . $mes . '/outros/' . $file->arquivo_hash;
        }
        return($path);
    }
    
    public static function image($file, $width, $heigth) {
        if (!$file) {
            return('');
        }
        
        if($file->arquivo_extensao !== 'jpg' && $file->arquivo_extensao !== 'jpeg') {
            return false;
        }
        
        $data = UtilHelper::formatDate($file->created_at);
        list($dia, $mes, $ano) = explode('/', $data);
        $path = '/storage/webdisco/' . $ano . '/' . $mes . '/jpg/' . $width . 'x' . $heigth . '/' . $file->id_arquivo . '/' . $file->arquivo_hash;
        return $path;
    }
}
