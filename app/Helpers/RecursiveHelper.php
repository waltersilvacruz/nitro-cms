<?php

namespace App\Helpers;

use DB;
use Session;

class RecursiveHelper
{
    const ORDERED_LIST = 1;
    const UNORDERED_LIST = 2;

    private $model;
    private $primaryKey;
    private $parentKey;
    private $label;
    private $orderBy;
    private $aditionalColumns;
    private $dataset = null;
    private $proccessedDataset = [];
    private $recursiveData = [];
    private $recursivePath = [];
    private $charLimit = false;
    private $extraWhere = [];
    private $extraWhereIn = [];

    /**
     * Constructor
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param mixed $model (optional)
     * @param mixed $primaryKey (optional)
     * @param mixed $parentKey (optional)
     * @param string $label (optional)
     * @param string $orderBy (optional)
     * @param array $aditionalColumns (optional)
     * @return void
     */
    public function __construct($model = null, $primaryKey = null, $parentKey = null, $label = null, $orderBy = null, $aditionalColumns = [])
    {
        $this->setModel($model)
            ->setPrimaryKey($primaryKey)
            ->setParentKey($parentKey)
            ->setLabel($label)
            ->setOrderBy($orderBy)
            ->setAditionalColumns($aditionalColumns);
    }

    /**
     * Get records as a simple array
     *
     * @param integer $order_type (optional)
     * @return array $list (optional)
     */
    public function getAsSimpleArray($parentId = 0, $order_type = self::ORDERED_LIST)
    {
        $this->recursiveSimpleArray($parentId);
        return $this->recursiveData;
    }

    /**
     * Get records as list
     *
     * @param integer $parentId
     * @param integer $list_type (optional)
     * @param array $options (optional)
     * @return array $list
     */
    public function getAsList($parentId = 0, $list_type = self::ORDERED_LIST, $options = [])
    {
        return $this->generateList($this->recursive($parentId), $list_type, $options);
    }

    /**
     * Get records as a complex array
     *
     * @param integer $primaryId (optional)
     * @param bool $reset (optional)
     * @return void
     */
    public function getRecursivePath($primaryId = 0, $divisor = ' / ', $reset = false)
    {
        $items = array_reverse($this->recursivePath($primaryId, $divisor, $reset));
        $arrPath = [];
        foreach($items as $item) {
            array_push($arrPath, $item['label']);
        }
        return str_replace(array('"', "'"), '', implode($divisor, $arrPath));
    }

    /**
     * Get an array by path
     *
     * @param integer $primaryId (optional)
     * @param bool $reset (optional)
     * @return void
     */
    public function getRecursivePathArray($primaryId = 0, $reset = false)
    {
        $path = $this->recursivePath($primaryId, $reset);
        return array_reverse($path);
    }

    /**
     * Generate a list from array
     *
     * @param mixed $recursiveData
     * @param integer $list_type
     * @param array $options (optional)
     * @param integer $ptr (optional)
     * @return array $list
     */
    private function generateList($recursiveData, $list_type, $options = [], $ptr = 0)
    {
        if($recursiveData === null || !is_array($recursiveData)) {
            return '';
        }
        $lt = ($list_type == self::ORDERED_LIST) ? 'ol' : 'ul';
        $list_item_class = (isset($options['list_item_class'])) ? $options['list_item_class'] : '';
        $output = "<" . $lt .">\n";
        foreach($recursiveData as $key => $value) {
            list($primaryId, $parentId, $label, $extraDataArray) = explode("|", $key);
            
            $aditionalData = unserialize($extraDataArray);
            $extraData = '';
            if(count($aditionalData) > 0) {
                foreach($aditionalData as $data => $val) {
                    $extraData .= ' data-' . str_replace('_', '-', $data) . '="' . $val . '"';
                }
            }
            
            $output .= '<li class="' . $list_item_class . '" data-id="' . $primaryId . '" data-parent-id="' . $parentId . '" ' . $extraData . '>' . $label;
            $output .= $this->generateList($value, $list_type, $options, $ptr);
            $output .= "</li>\n";
        }
        $output .= '</' . $lt . '>';
        return $output;
    }

    /**
     * Get records as a simple array
     *
     * @param integer $parentId (optional)
     * @param integer $ptr (optional)
     * @return void
     */
    private function recursiveSimpleArray($parentId = 0, $ptr = 0)
    {
        if($this->getDataset() === null) {
            $this->setDataset($this->fetchRecords());
        }

        $total = $this->totalChildrens($parentId);
        $ptr = ($total > 0) ? $ptr + 1 : $ptr - 1;
        if($total == 0) {
            return;
        }

        foreach($this->getDataset() as $rs) {
            if($rs['parentId'] != $parentId) {
                continue;
            }
            if(!in_array($rs['primaryId'], $this->proccessedDataset)) {
                $output = '';
                array_push($this->proccessedDataset, $rs['primaryId']);

                if($this->getCharLimit()) {
                    $output .= strlen($rs['label']) > $this->getCharLimit() ? substr($rs['label'], 0, $this->getCharLimit()) . '...' : $rs['label'];
                } else {
                    $output .= $rs['label'];
                }
                $item = [];
                $item['label'] = $output;
                $item['parentId'] = $rs['parentId'];
                $item['primaryId'] = $rs['primaryId'];
                $item['tab'] = ($ptr - 1);
                foreach($this->getAditionalColumns() as $col) {
                    $item[$col] = $rs[$col];
                }

                array_push($this->recursiveData, $item);
                $this->recursiveSimpleArray( $rs['primaryId'], $ptr);
            }
        }
        return;
    }

    /**
     * Get records as a complex array
     *
     * @param integer $parentId (optional)
     * @param integer $ptr (optional)
     * @return void
     */
    private function recursive($parentId = 0, $ptr = 0)
    {
        if($this->getDataset() === null) {
            $this->setDataset($this->fetchRecords());
        }

        $total = $this->totalChildrens($parentId);
        $ptr = ($total > 0) ? $ptr + 1 : $ptr - 1;
        if($total == 0) {
            return;
        }

        $outputList = [];

        foreach($this->getDataset() as $rs) {
            if($rs['parentId'] != $parentId) {
                continue;
            }
            if(!in_array($rs['primaryId'], $this->proccessedDataset)) {
                $output = '';
                array_push($this->proccessedDataset, $rs['primaryId']);

                if($this->getCharLimit()) {
                    $output .= strlen($rs['label']) > $this->getCharLimit() ? substr($rs['label'], 0, $this->getCharLimit()) . '...' : $rs['label'];
                } else {
                    $output .= $rs['label'];
                }
                $extraDataArray = [];
                foreach($this->getAditionalColumns() as $col) {
                    $extraDataArray[$col] = $rs[$col];
                }
                $outputList[$rs['primaryId'] . '|' . $rs['parentId'] . '|' . $output . '|' . serialize($extraDataArray)] = $this->recursive( $rs['primaryId'], $ptr);
            }
        }
        return $outputList;
    }

    /**
     * Get records as a complex array
     *
     * @param integer $primaryId (optional)
     * @param bool $reset (optional)
     * @return void
     */
    private function recursivePath($primary = 0, $divisor = ' / ', $reset = false)
    {
        if($this->getDataset() === null) {
            $this->setDataset($this->fetchRecords());
        }

        if($reset && strlen($this->recursivePath) > 0) {
            $this->recursivePath = [];
        }

        $item = $this->getRecord($primary);
        if($item) {
            array_push($this->recursivePath, $item);
            if($item['parentId'] > 0) {
                $this->recursivePath($item['parentId'], $divisor, $reset);
            }
        }
        return($this->recursivePath);
    }

    /**
     * Retrieve all records from database
     *
     * @return array $recordset
     */
    private function fetchRecords()
    {
         $dataset = [];
         if(!$this->getModel() || !$this->getPrimaryKey() || !$this->getParentKey() || !$this->getLabel() || !$this->getOrderBy()) {
             throw new Exception("Parâmetros incompletos para realização da instrução SQL");
         }

         try {
             $modelClass = $this->getModel();
             $recordset = $modelClass::orderBy($this->getOrderBy(), 'asc');
             //$recordset = $modelClass::where('id_diretorio', '<>', '744')->orderBy($this->getOrderBy(), 'asc');
             if(count($this->extraWhere) > 0) {
                 foreach($this->extraWhere as $where) {
                     $recordset = $recordset->where($where['column'], $where['operator'], $where['value']);
                 }
             }
             foreach($recordset->get()->toArray() as $rs) {
                 $item = [
                     'primaryId' => $rs[$this->getPrimaryKey()],
                     'parentId' => $rs[$this->getParentKey()] ? $rs[$this->getParentKey()] : 0,
                     'label' => $rs[$this->getLabel()]
                 ];

                 foreach($this->getAditionalColumns() as $col) {
                     $item[$col] = $rs[$col];
                 }
                 array_push($dataset, $item);
             }
         } catch (Exception $e) {
             throw new Exception($e->getMessage());
         }

         return $dataset;
    }

    /**
     * Reset the dataset
     *
     * @return void
     */
    public function resetDataset()
    {
      $this->setDataset(null);
      $this->recursiveData = [];
      $this->proccessedDataset = [];
      return $this;
    }

    /**
     * Count total childrens
     *
     * @param mixed $parentId
     * @return integer $total
     */
    private function totalChildrens($parentId) {
        $tot = 0;
        foreach ($this->getDataset() as $rs) {
            if ($rs['parentId'] == $parentId && !in_array($rs['primaryId'], $this->proccessedDataset)) {
                $tot++;
            }
        }
        return $tot;
    }

    /**
     * Retrieve a cached record by its id
     *
     * @param integer $primaryId
     * @return mixed $record
     */
    private function getRecord($primaryId)
    {
        $ret = false;
        foreach($this->getDataset() as $item) {
            if($item['primaryId'] == $primaryId) {
                $ret = $item;
                break;
            }
        }
        return $item;
    }

    /**
     * Set a new filter to sql
     *
     * @param string $column
     * @param string $operator
     * @param string $value
     */
    public function addWhere($column, $operator, $value)
    {
        array_push($this->extraWhere, [
            'column' => $column,
            'operator' => $operator,
            'value' => $value
        ]);

        return $this;
    }
    
    /**
     * Set a new filter to sql
     *
     * @param string $column
     * @param string $value
     */
    public function addWhereIn($column, $value)
    {
        array_push($this->extraWhereIn, [
            'column' => $column,
            'value' => $value
        ]);

        return $this;
    }

    /**
     * Get the value of Model
     *
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set the value of Model
     *
     * @param mixed model
     *
     * @return self
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get the value of Primary Key
     *
     * @return mixed
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * Set the value of Primary Key
     *
     * @param mixed primaryKey
     *
     * @return self
     */
    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = $primaryKey;

        return $this;
    }

    /**
     * Get the value of Parent Key
     *
     * @return mixed
     */
    public function getParentKey()
    {
        return $this->parentKey;
    }

    /**
     * Set the value of Parent Key
     *
     * @param mixed parentKey
     *
     * @return self
     */
    public function setParentKey($parentKey)
    {
        $this->parentKey = $parentKey;

        return $this;
    }


    /**
     * Get the value of Label
     *
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set the value of Label
     *
     * @param mixed label
     *
     * @return self
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }


    /**
     * Get the value of Order By
     *
     * @return mixed
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * Set the value of Order By
     *
     * @param mixed orderBy
     *
     * @return self
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }


    /**
     * Get the value of Dataset
     *
     * @return mixed
     */
    public function getDataset()
    {
        return $this->dataset;
    }

    /**
     * Set the value of Dataset
     *
     * @param mixed dataset
     *
     * @return self
     */
    public function setDataset($dataset)
    {
        $this->dataset = $dataset;

        return $this;
    }


    /**
     * Get the value of Char Limit
     *
     * @return mixed
     */
    public function getCharLimit()
    {
        return $this->charLimit;
    }

    /**
     * Set the value of Char Limit
     *
     * @param mixed charLimit
     *
     * @return self
     */
    public function setCharLimit($charLimit)
    {
        $this->charLimit = $charLimit;

        return $this;
    }


    /**
     * Get the value of Aditional Columns
     *
     * @return mixed
     */
    public function getAditionalColumns()
    {
        return $this->aditionalColumns;
    }

    /**
     * Set the value of Aditional Columns
     *
     * @param mixed aditionalColumns
     *
     * @return self
     */
    public function setAditionalColumns($aditionalColumns)
    {
        $this->aditionalColumns = $aditionalColumns;

        return $this;
    }
}
