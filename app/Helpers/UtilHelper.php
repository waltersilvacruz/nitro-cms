<?php

namespace App\Helpers;

class UtilHelper {

    public static $mensagemValidacao = [
        'required' => '<strong><u>:attribute</u></strong> é obrigatório',
        'numeric' => '<strong><u>:attribute</u></strong> precisa ser um número válido',
        'max' => '<strong><u>:attribute</u></strong> não pode ter mais que :max',
        'min' => '<strong><u>:attribute</u></strong> não pode ter menos que :min',
        'email' => '<strong><u>:attribute</u></strong> não é um email válido',
        'date' => '<strong><u>:attribute</u></strong> não é uma data válida',
        'between' => '<strong><u>:attribute</u></strong> deve ser entre :min e :max',
        'confirmed' => 'A confirmação do campo <strong><u>:attribute</u></strong> falhou.',
    ];

    public static function formatNumber($input) {
        if (strlen($input) == 0) {
            return;
        }

        if (strpos($input, ',') === false) {
            return number_format($input, 2, ',', '.');
        } else {
            return str_replace(',', '.', str_replace('.', '', $input));
        }
    }
    
    public static function getTimestamp($data) {
        $ts = new \DateTime($data);
        return $ts->format('U');
    }

    public static function formatDate($input, $mostraHora = false) {
        if ($input == '') {
            return;
        }

        if (strpos($input, ' ') !== false) {
            list($data, $hora) = explode(' ', $input);
        } else {
            $data = $input;
            $hora = false;
        }

        if (strpos($data, '-')) {
            list($ano, $mes, $dia) = explode('-', $data);
            if ($mostraHora && $hora) {
                return "$dia/$mes/$ano " . substr($hora, 0, 5);
            } else {
                return "$dia/$mes/$ano";
            }
        } else {
            list($dia, $mes, $ano) = explode('/', $data);
            if ($mostraHora && $hora) {
                return "$ano-$mes-$dia " . substr($hora, 0, 5);
            } else {
                return "$ano-$mes-$dia";
            }
        }
    }

    public static function validaCPF($cpf = null) {

        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        elseif ($cpf == '00000000000' ||
                $cpf == '11111111111' ||
                $cpf == '22222222222' ||
                $cpf == '33333333333' ||
                $cpf == '44444444444' ||
                $cpf == '55555555555' ||
                $cpf == '66666666666' ||
                $cpf == '77777777777' ||
                $cpf == '88888888888' ||
                $cpf == '99999999999') {
            return false;
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }

    public static function validaCNPJ($cnpj = null) {
        if (empty($cnpj)) {
            return false;
        }
        //Etapa 1: Cria um array com apenas os digitos numéricos, isso permite receber o cnpj em diferentes formatos como "00.000.000/0000-00", "00000000000000", "00 000 000 0000 00" etc...
        $j = 0;
        for ($i = 0; $i < (strlen($cnpj)); $i++) {
            if (is_numeric($cnpj[$i])) {
                $num[$j] = $cnpj[$i];
                $j++;
            }
        }
        //Etapa 2: Conta os dígitos, um Cnpj válido possui 14 dígitos numéricos.
        if (count($num) != 14) {
            $isCnpjValid = false;
        }
        //Etapa 3: O número 00000000000 embora não seja um cnpj real resultaria um cnpj válido após o calculo dos dígitos verificares e por isso precisa ser filtradas nesta etapa.
        if ($num[0] == 0 && $num[1] == 0 && $num[2] == 0 && $num[3] == 0 && $num[4] == 0 && $num[5] == 0 && $num[6] == 0 && $num[7] == 0 && $num[8] == 0 && $num[9] == 0 && $num[10] == 0 && $num[11] == 0) {
            $isCnpjValid = false;
        }
        //Etapa 4: Calcula e compara o primeiro dígito verificador.
        else {
            $j = 5;
            for ($i = 0; $i < 4; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 4; $i < 12; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[12]) {
                $isCnpjValid = false;
            }
        }
        //Etapa 5: Calcula e compara o segundo dígito verificador.
        if (!isset($isCnpjValid)) {
            $j = 6;
            for ($i = 0; $i < 5; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 5; $i < 13; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[13]) {
                $isCnpjValid = false;
            } else {
                $isCnpjValid = true;
            }
        }

        return $isCnpjValid;
    }

    public static function mask($val, $mask) {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k])) {
                    $maskared .= $val[$k++];
                }
            } else {
                if (isset($mask[$i])) {
                    $maskared .= $mask[$i];
                }
            }
        }
        return $maskared;
    }

    public static function filesize($bytes) {

        $unArr = array('bytes', 'KB', 'MB', 'GB', 'TB');
        if ($bytes == '') {
            $bytes = 0;
        }
        $ret = '';
        foreach ($unArr as $a) {
            if ($bytes > 1024) {
                $bytes = ($bytes / 1024 );
            } else {
                $ret = sprintf("%.1f %s", $bytes, $a);
                break;
            }
        }
        return($ret);
    }

    public static function friendly($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
    
    public static function slugify($model, $text, $forceTrashed = true) {
        if (empty($text)) {
            $text = 'N/A';
        }

        $slug = self::friendly($text);
        $slug_cnt = 1;
        while (1) {
            if($forceTrashed) {
                $model_slug = $model->withTrashed()->where('slug', $slug)->first();
            } else {
                $model_slug = $model->where('slug', $slug)->first();
            }
            if ($model_slug) {
                $slug = $text . '-' . $slug_cnt++;
            } else {
                break;
            }
        }
        return $slug;
    }

}
