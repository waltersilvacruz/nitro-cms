<?php

namespace App\Http\Controllers\Secao;

use App\Http\Controllers\Controller;
use App\Databases\Repositories\DB\SecaoRepository;
use Illuminate\Http\Request;
use App\Helpers\UtilHelper;
use Response;

class SecaoController extends Controller {

    private $secaoRepository;

    public function __construct(SecaoRepository $secaoRepository) {
        $this->secaoRepository = $secaoRepository;
    }

    public function index() {
        $secoes = $this->secaoRepository->getAll();
        return view('secao.index', [
            'secoes' => $secoes
        ]);
    }
    
    public function novo() {
        return view('secao.novo');
    }
    
    public function create(Request $request) {
        $validacao = [
            'nome' => 'required|max:100',
            'descricao' => 'max:4000'
        ];
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $secao = $this->secaoRepository->create($request->all());
        return Response::json($secao);
    }
    
    public function edita($id_secao) {
        $secao = $this->secaoRepository->getById($id_secao);
        return view('secao.edita', [
            'secao' => $secao
        ]);
    }
    
    public function update(Request $request, $id_secao) {
        $validacao = [
            'nome' => 'required|max:100',
            'descricao' => 'max:4000'
        ];
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $secao = $this->secaoRepository->update($id_secao, $request->all());
        return Response::json($secao);
    }
    
    public function destroy(Request $request, $id_secao) {
        $this->secaoRepository->destroy($id_secao);
        return Response::json([]);
    }
}
