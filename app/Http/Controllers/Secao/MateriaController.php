<?php

namespace App\Http\Controllers\Secao;

use App\Http\Controllers\Controller;
use App\Databases\Repositories\DB\SecaoRepository;
use App\Databases\Repositories\DB\MateriaRepository;
use Illuminate\Http\Request;
use App\Helpers\UtilHelper;
use Response;

class MateriaController extends Controller {

    private $secaoRepository;
    private $materiaRepository;

    public function __construct(SecaoRepository $secaoRepository, MateriaRepository $materiaRepository) {
        $this->secaoRepository = $secaoRepository;
        $this->materiaRepository = $materiaRepository;
    }

    public function index(Request $request, $id_secao) {
        $secao = $this->secaoRepository->getById($id_secao);
        return view('secao.materia.index', [
            'id_secao' => $id_secao,
            'secao' => $secao
        ]);
    }
    
    public function paginate(Request $request, $id_secao) {
        $data = $request->all();
        $result = $this->materiaRepository->paginate($id_secao, $data);
        return Response::json($result);
    }
    
    public function novo(Request $request, $id_secao) {
        $secao = $this->secaoRepository->getById($id_secao);
        return view('secao.materia.novo', [
            'id_secao' => $id_secao,
            'secao' => $secao
        ]);
    }
    
    public function create(Request $request, $id_secao) {
        $validacao['titulo'] = 'required|max:200';
        $validacao['data'] = 'required|max:10';
        $validacao['hora'] = 'required|max:5';
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $materia = $this->materiaRepository->create($request->all(),$id_secao);
        return Response::json($materia);
    }
    
    public function edit(Request $request, $id_secao, $id_materia) {
        $materia = $this->materiaRepository->get($id_secao, $id_materia);
        $secao = $this->secaoRepository->getById($id_secao);
        $tags = $this->materiaRepository->getTags($id_materia);
        $anexos = $this->materiaRepository->getAnexos($id_materia);
        
        $arrAnexos = [];
        foreach($anexos as $anexo) {
            array_push($arrAnexos, $anexo->id_arquivo);
        }
        
        $arrTags = [];
        foreach($tags as $tag) {
            array_push($arrTags, $tag->tag);
        }
        
        return view('secao.materia.edita', [
            'materia' => $materia,
            'secao' => $secao,
            'tags' => implode(",", $arrTags),
            'anexos' => implode(',', $arrAnexos)
        ]);
    }
    
    public function update(Request $request, $id_secao, $id_materia) {
        $validacao['titulo'] = 'required|max:200';
        $validacao['data'] = 'required|max:10';
        $validacao['hora'] = 'required|max:5';
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $materia = $this->materiaRepository->update($request->all(),$id_secao, $id_materia);
        return Response::json($materia);
    }
    
    public function destroy(Request $request, $id_secao, $id_materia) {
        $this->materiaRepository->destroy($request->all(), $id_secao, $id_materia);
        return Response::json([]);
    }
}