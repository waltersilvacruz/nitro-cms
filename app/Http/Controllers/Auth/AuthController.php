<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Response;
use Illuminate\Http\Request;
use Session;
use Auth;
use Hash;
use Validator;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';


    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    public function index(Request $request) {
        return view('login');
    }

    public function authenticate(Request $request)
    {
        $credencial = ['logon' => $request->input('logon'), 'password' => $request->input('password')];

        $validator = Validator::make(
            $credencial, [
                'password' => 'required|min:3',
                'logon' => 'required'
            ], [
                'required' => ':attribute é obrigatório',
                'min' => ':attribute deve ter ao menos :min caracteres'
            ]
        );

        if ($validator->fails()) {
            return redirect('login')->withErrors($validator)->withInput();
        } else {
            if (Auth::attempt($credencial)) {
                    return redirect()->intended('home');
            } else {
                $validator->errors()->add('erro', 'Login ou senha inválida');
                return redirect('login')->withErrors($validator)->withInput();
            }
        }
    }

    public function logout() {
        Session::flush();
        return redirect('/');
    }

    public function ping() {
        return Response::json(['timestamp' => time()]);
    }
}
