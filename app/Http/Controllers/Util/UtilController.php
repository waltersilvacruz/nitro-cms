<?php

namespace App\Http\Controllers\Util;

use App\Http\Controllers\Controller;
use App\Databases\Repositories\DB\TagRepository;
use Illuminate\Http\Request;
use Response;
use Image;

class UtilController extends Controller {

    public function __construct() {
        
    }

    public function randomImage(Request $request, $w, $h) {
        $source = sprintf("%s/webdev/builder/assets/img/%03d.jpg", public_path(), rand(1, 30));
        
        $image = Image::make($source);
        $width = $image->width();
        $height = $image->height();

        if ($w > $h) {
            $image->resize($w, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        } else if($h > $w) {
            $image->resize(null, $h, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        } else {
            $image->resize(null, $h, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }
        return $image->greyscale()->brightness(5)->crop($w, $h)->blur(0)->response('jpg');
    }

    public function tags(Request $request) {
        $tagRepository = new TagRepository();
        $tags = $tagRepository->search($request->all());
        return Response::json($tags);
    }

}
