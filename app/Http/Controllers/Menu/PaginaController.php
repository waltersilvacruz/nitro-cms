<?php

namespace App\Http\Controllers\Menu;

use App\Http\Controllers\Controller;
use App\Databases\Repositories\DB\MenuRepository;
use App\Databases\Repositories\DB\PaginaRepository;
use App\Databases\Repositories\DB\SecaoRepository;
use App\Databases\Models\SecaoModel;
use App\Databases\Repositories\DB\ArticulistaRepository;
use App\Databases\Models\ArticulistaModel;
use Illuminate\Http\Request;
use App\Helpers\RecursiveHelper;
use App\Helpers\UtilHelper;
use Response;

class PaginaController extends Controller {

    private $menuRepository;
    private $paginaRepository;

    public function __construct(MenuRepository $menuRepository, PaginaRepository $paginaRepository) {
        $this->menuRepository = $menuRepository;
        $this->paginaRepository = $paginaRepository;
    }

    public function index(Request $request, $id_menu) {
        return view('menu.pagina.index', [
            'id_menu' => $id_menu
        ]);
    }
    
    public function treeview(Request $request, $id_menu) {
        $menu = $this->menuRepository->getById($id_menu);
        $rq = new RecursiveHelper('App\Databases\Models\VwPaginaModel', 'id_pagina', 'id_pagina_superior', 'titulo', 'ordem');
        $paginas = $rq->addWhere('id_menu', '=', $id_menu)
                ->setAditionalColumns(['tipo_pagina', 'publicado', 'ordem'])
                ->getAsList(-1, $rq::UNORDERED_LIST, []);
        
        return view('menu.pagina.treeview', [
            'paginas' => $paginas,
            'menu' => $menu
        ]);
    }
    
    public function create(Request $request, $id_menu) {
        $pagina = $this->paginaRepository->create($request->all(), $id_menu);
        return Response::json($pagina);
    }
    
    public function edit(Request $request, $id_menu, $id_pagina) {
        $pagina = $this->paginaRepository->get($id_menu, $id_pagina);
        $anexos = $this->paginaRepository->getAnexos($id_pagina);
        $arrAnexos = [];
        
        foreach($anexos as $anexo) {
            array_push($arrAnexos, $anexo->id_arquivo);
        }
        
        // secoes de conteudo
        $secaoRepository = new SecaoRepository(new SecaoModel());
        
        // articulistas
        $articulistaRepository = new ArticulistaRepository(new ArticulistaModel());
        
        return view('menu.pagina.edita', [
            'pagina' => $pagina,
            'anexos' => implode(',', $arrAnexos),
            'secoes' => $secaoRepository->getAll(),
            'articulistas' => $articulistaRepository->getAll()
        ]);
    }
    
    public function update(Request $request, $id_menu, $id_pagina) {
        $validacao['titulo'] = 'required|max:200';
        
        if($request->get('tipo_pagina') == 1) {
            $validacao['texto'] = 'required';
        } else if($request->get('tipo_pagina') == 3) {
            $validacao['redirect'] = 'required|max:250';
        }
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $pagina = $this->paginaRepository->update($request->all(),$id_menu, $id_pagina);
        return Response::json($pagina);
    }
    
    public function rename(Request $request, $id_menu, $id_pagina) {
        $validacao['titulo'] = 'required|max:200';
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $pagina = $this->paginaRepository->rename($request->all(), $id_menu, $id_pagina);
        return Response::json($pagina);
    }
    
    public function destroy(Request $request, $id_menu, $id_pagina) {
        $this->paginaRepository->destroy($request->all(), $id_menu, $id_pagina);
        return Response::json([]);
    }
    
    public function sort(Request $request, $id_menu, $id_pagina) {
        $this->paginaRepository->sort($request->all(), $id_menu, $id_pagina);
        return Response::json([]);
    }
}
