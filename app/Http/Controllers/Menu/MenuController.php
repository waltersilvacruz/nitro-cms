<?php

namespace App\Http\Controllers\Menu;

use App\Http\Controllers\Controller;
use App\Databases\Repositories\DB\MenuRepository;
use Illuminate\Http\Request;
use App\Helpers\UtilHelper;
use Response;

class MenuController extends Controller {

    private $menuRepository;

    public function __construct(MenuRepository $menuRepository) {
        $this->menuRepository = $menuRepository;
    }

    public function index() {
        $menus = $this->menuRepository->getAll();
        return view('menu.index', [
            'menus' => $menus
        ]);
    }
    
    public function novo() {
        return view('menu.novo');
    }
    
    public function create(Request $request) {
        $validacao = [
            'tipo' => 'required',
            'nome' => 'required|max:100',
            'descricao' => 'max:4000'
        ];
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $menu = $this->menuRepository->create($request->all());
        return Response::json($menu);
    }
    
    public function edita($id_menu) {
        $menu = $this->menuRepository->getById($id_menu);
        return view('menu.edita', [
            'menu' => $menu
        ]);
    }
    
    public function update(Request $request, $id_menu) {
        $validacao = [
            'tipo' => 'required',
            'nome' => 'required|max:100',
            'descricao' => 'max:4000'
        ];
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $menu = $this->menuRepository->update($id_menu, $request->all());
        return Response::json($menu);
    }
    
    public function destroy(Request $request, $id_menu) {
        $this->menuRepository->destroy($id_menu);
        return Response::json([]);
    }
}
