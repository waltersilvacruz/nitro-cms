<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;
use App\Databases\Repositories\DB\TemplateRepository;
use Illuminate\Http\Request;
use Response;

class BuilderController extends Controller {

    private $templateRepository;

    public function __construct(TemplateRepository $templateRepository) {
        $this->templateRepository = $templateRepository;
    }

    public function index(Request $request) {
        $templates = $this->templateRepository->getAll();
        return view('builder.index', [
            'templates' => $templates
        ]);
    }

    public function editor(Request $request, $id_template = null) {
        $encrypter = app('Illuminate\Encryption\Encrypter');
        $encrypted_token = $encrypter->encrypt(csrf_token());
        if ($id_template) {
            $template = $this->templateRepository->getById($id_template);
        }
        return view('builder.editor', [
            'token' => $encrypted_token,
            'id_template' => $id_template,
            'template' => $id_template ? $template : null
        ]);
    }

    public function load(Request $request, $id_template) {
        $template = $this->templateRepository->getById($id_template);
        if (!$template) {
            abort(404);
        }
        $configuracao = json_decode($template->configuracao, true);
        return Response::json([
                    'nome' => $template->nome,
                    'descricao' => $template->descricao,
                    'configuracao' => $configuracao
        ]);
    }

    public function layouts() {
        $result = [];
        $config = file_get_contents(public_path() . '/webdev/builder/layouts.json');
        if (!$config) {
            abort(500);
        }
        $json = json_decode($config, true);
        $newConfig = [];
        foreach ($json['layouts'] as $layout) {
            $template = file_get_contents(public_path() . '/webdev/builder/templates/' . $layout['templateFile']);
            if (!$template) {
                continue;
            }

            $layout['template'] = $template;
            array_push($newConfig, $layout);
        }
        $json['layouts'] = $newConfig;
        return Response::json($json);
    }

    public function create(Request $request) {
        //$input = $request->getContent();
        $input = [
            'nome' => $request->json()->get('nome'),
            'descricao' => $request->json()->get('descricao'),
            'configuracao' => json_encode($request->json()->get('configuracao')),
            'thumbnail' => $request->json()->get('thumbnail'),
            'publicado' => $request->json()->get('publicado')
        ];
        $template = $this->templateRepository->create($input);
        return Response::json($template);
    }

    public function update(Request $request, $id_template) {
        //$input = $request->getContent();
        $input = [
            'nome' => $request->json()->get('nome'),
            'descricao' => $request->json()->get('descricao'),
            'configuracao' => json_encode($request->json()->get('configuracao')),
            'thumbnail' => $request->json()->get('thumbnail'),
            'publicado' => $request->json()->get('publicado')
        ];
        $template = $this->templateRepository->update($id_template, $input);
        return Response::json($template);
    }

    public function destroy(Request $request, $id_template) {
        $this->templateRepository->destroy($id_template);
        return Response::json([]);
    }

}
