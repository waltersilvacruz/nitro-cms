<?php

namespace App\Http\Controllers\Artigo;

use App\Http\Controllers\Controller;
use App\Databases\Repositories\DB\ArticulistaRepository;
use Illuminate\Http\Request;
use App\Helpers\UtilHelper;
use Response;

class ArticulistaController extends Controller {

    private $articulistaRepository;

    public function __construct(ArticulistaRepository $articulistaRepository) {
        $this->articulistaRepository = $articulistaRepository;
    }

    public function index() {
        $articulistas = $this->articulistaRepository->getAll();
        return view('artigo.articulista.index', [
            'articulistas' => $articulistas
        ]);
    }
    
    public function novo() {
        return view('artigo.articulista.novo');
    }
    
    public function create(Request $request) {
        $validacao = [
            'foto' => 'required',
            'nome' => 'required|max:50',
            'resumo' => 'max:4000'
        ];
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $articulista = $this->articulistaRepository->create($request->all());
        return Response::json($articulista);
    }
    
    public function edita($id_articulista) {
        $articulista = $this->articulistaRepository->getById($id_articulista);
        if(!$articulista) {
            abort(404);
        }
        return view('artigo.articulista.edita', [
            'articulista' => $articulista
        ]);
    }
    
    public function update(Request $request, $id_articulista) {
        $validacao = [
            'foto' => 'required',
            'nome' => 'required|max:50',
            'resumo' => 'max:4000'
        ];
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $articulista = $this->articulistaRepository->update($id_articulista, $request->all());
        return Response::json($articulista);
    }
    
    public function destroy(Request $request, $id_articulista) {
        $this->articulistaRepository->destroy($id_articulista);
        return Response::json([]);
    }
}
