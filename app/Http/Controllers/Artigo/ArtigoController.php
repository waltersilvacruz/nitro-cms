<?php

namespace App\Http\Controllers\Artigo;

use App\Http\Controllers\Controller;
use App\Databases\Repositories\DB\ArtigoRepository;
use App\Databases\Repositories\DB\ArticulistaRepository;
use Illuminate\Http\Request;
use App\Helpers\UtilHelper;
use Response;

class ArtigoController extends Controller {

    private $artigoRepository;
    private $articulistaRepository;

    public function __construct(ArtigoRepository $artigoRepository, ArticulistaRepository $articulistaRepository) {
        $this->artigoRepository = $artigoRepository;
        $this->articulistaRepository = $articulistaRepository;
    }

    public function index() {
        $artigos = $this->artigoRepository->getAll();
        return view('artigo.index', [
            'artigos' => $artigos
        ]);
    }
    
    public function novo() {
        $articulistas = $this->articulistaRepository->getAll();
        return view('artigo.novo', [
            'articulistas' => $articulistas
        ]);
    }
    
    public function create(Request $request) {
        $validacao = [
            'articulista' => 'required',
            'data' => 'required',
            'hora' => 'required',
            'titulo' => 'required|max:100',
            'texto' => 'required'
        ];
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $artigo = $this->artigoRepository->create($request->all());
        return Response::json($artigo);
    }
    
    public function edita($id_artigo) {
        $articulistas = $this->articulistaRepository->getAll();
        $artigo = $this->artigoRepository->getById($id_artigo);
        
        if(!$artigo) {
            abort(404);
        }
        
        $tags = $this->artigoRepository->getTags($id_artigo);
        $anexos = $this->artigoRepository->getAnexos($id_artigo);
        
        $arrAnexos = [];
        foreach($anexos as $anexo) {
            array_push($arrAnexos, $anexo->id_arquivo);
        }
        
        $arrTags = [];
        foreach($tags as $tag) {
            array_push($arrTags, $tag->tag);
        }
        return view('artigo.edita', [
            'artigo' => $artigo,
            'articulistas' => $articulistas,
            'tags' => implode(",", $arrTags),
            'anexos' => implode(',', $arrAnexos)
        ]);
    }
    
    public function update(Request $request, $id_artigo) {
        $validacao = [
            'articulista' => 'required',
            'data' => 'required',
            'hora' => 'required',
            'titulo' => 'required|max:100',
            'texto' => 'required'
        ];
        
        $this->validate($request, $validacao, UtilHelper::$mensagemValidacao);
        $artigo = $this->artigoRepository->update($id_artigo, $request->all());
        return Response::json($artigo);
    }
    
    public function destroy(Request $request, $id_artigo) {
        $this->artigoRepository->destroy($id_artigo);
        return Response::json([]);
    }
}
