<?php

namespace App\Http\Controllers\WebDisco;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\RecursiveHelper;
use App\Helpers\StorageHelper;
use App\Helpers\UtilHelper;
use App\Databases\Repositories\DB\DiretorioRepository;
use App\Databases\Repositories\DB\ArquivoRepository;
use App;
use Input;
use Response;
use DB;
use Storage;
use Image;

class WebdiscoController extends Controller {
    private $dirRepository;
    private $fileRepository;
    
    public function __construct(DiretorioRepository $repo, ArquivoRepository $fileRepository) {
        $this->dirRepository = $repo;
        $this->fileRepository = $fileRepository;
    }

    public function index(Request $request) {
        return view('webdisco.index', [
            'layout' => $request->get('popup', false) ? 'webdisco.popup' : 'layout',
            'popup' => $request->get('popup', false) ? 'S' : 'N',
            'filtro' => $request->get('filtro', 'all'),
            'target' => $request->get('target', '')
        ]);
    }

    public function treeview(Request $request) {
        $rq = new RecursiveHelper('App\Databases\Models\DiretorioModel', 'id_diretorio', 'id_diretorio_superior', 'nome', 'nome');
        $diretorios = $rq->getAsList(-1, $rq::UNORDERED_LIST, []);
        return view('webdisco.treeview', [
            'diretorios' => $diretorios
        ]);
    }

    public function create() {
        $parent = Input::get('parent');
        try {
            $id = $this->dirRepository->create(Input::all());
        } catch (Exception $ex) {
            App::abort(500, $ex->getMessage());
        }
        return Response::json([
            'id' => $id,
            'parent' => $parent
        ]);
    }

    public function update() {
        try {
            $this->dirRepository->update(Input::all());
        } catch (Exception $ex) {
            DB::rollback();
            App::abort(500, $ex->getMessage());
        }
        return Response::json([]);
    }
    
    public function destroy() {
        try {
            $this->dirRepository->destroy(Input::all());
        } catch (Exception $ex) {
            DB::rollback();
            App::abort(500, $ex->getMessage());
        }
        return Response::json([]);
    }
    
    public function files(Request $request, $id_diretorio) {
        if($id_diretorio == -1) {
            return [
                'files' => [],
                'info' => [
                    'path' => 'TCE-MT'
                ]
            ];
        }
        $files = $this->fileRepository->listFiles($id_diretorio, $request->get('filtro', 'all'));
        $ret = [
            'files' => $files,
            'info' => [
                'path' => '/'
            ]
        ];
        return Response::json($ret);
    }
    
    public function file(Request $request, $id_arquivo) {
        $arrFiles = [];
        if(strpos($id_arquivo, ',') !== false) {
            $arrFiles = explode(',', $id_arquivo);
        } else {
            array_push($arrFiles, $id_arquivo);
        }
        $files = $this->fileRepository->listFiles($arrFiles, $request->get('filtro', 'all'));
        $ret = [
            'files' => $files,
            'info' => [
                'path' => '/'
            ]
        ];
        return Response::json($ret);
    }
    
    public function fileinfo(Request $request, $id_diretorio, $id_arquivo) {
        $file = $this->fileRepository->info($id_arquivo);
        $preview = 'webdisco.preview.default';
        switch($file->arquivo_extensao) {
            case 'pdf':
                $preview = 'webdisco.preview.pdf';
                break;
            case 'jpg':
                $preview = 'webdisco.preview.image';
                break;
            case 'jpeg':
                $preview = 'webdisco.preview.image';
                break;
            case 'mp4':
                $preview = 'webdisco.preview.video';
                break;
            case 'flv':
                $preview = 'webdisco.preview.video';
                break;
            default:
                break;
        }
        $thumb = null;
        if($file->arquivo_extensao === 'jpg' || $file->arquivo_extensao === 'jpeg') {
            $data = UtilHelper::formatDate($file->created_at);
            list($dia, $mes, $ano) = explode('/', $data);
            $thumb = sprintf("%s/%04d/%02d/jpg/300x225/%d/%s", env('WEBDISCO_BASE_URL'), $ano, $mes, $id_arquivo, $file->arquivo_hash);
        }
        
        return view('webdisco.fileinfo', [
            'preview' => $preview,
            'path' => $request->get('path'),
            'file' => $file,
            'thumb' => $thumb,
            'popup' => $request->get('popup', 'N'),
            'filtro' => $request->get('filtro', 'all')
        ]);
    }
    
    public function download(Request $request, $id_arquivo) {
        $storage = Storage::disk(env('WEBDISCO_STORAGE_DRIVER'));
        $file = $this->fileRepository->info($id_arquivo);
        $file_url = StorageHelper::filePath($file);
        $file_path = StorageHelper::filePath($file, false);
        if($request->get('preview')) {
            if($storage->exists($file_path)) {
                return redirect($file_url, 302);
            } else {
                App::abort(404, $file_path);
            }
        } else {
            $source = $storage->get($file_path);
            return (new \Illuminate\Http\Response($source, 200))
                    ->header('Content-Type', $file->arquivo_mime_type)
                    ->header('Content-Disposition', 'attachment;filename=' . $file->arquivo_hash);
        }
    }
    
    public function fileDestroy() {
        try {
            $this->fileRepository->destroy(Input::all());
        } catch (Exception $ex) {
            App::abort(500, $ex->getMessage());
        }
        return Response::json([]);
    }
    
    public function search(Request $request) {
        if(!$request->get('keyword')) {
            return [
                'fiiles' => [],
                'info' => [
                    'path' => 'TCE-MT'
                ]
            ];
        }
        $files = $this->fileRepository->searchFiles($request->get('keyword'));
        $ret = [
            'files' => $files,
            'info' => [
                'path' => 'Pesquisa'
            ]
        ];
        return Response::json($ret);
    }
    
    public function upload($id_diretorio) {
        return view('webdisco.upload', [
            'id_diretorio' => $id_diretorio
        ]);
    }
    
    public function uploadCreate(Request $request, $id_diretorio) {
        $fileInfo = $this->uploadProcessa($request, $id_diretorio);
        $this->fileRepository->create($fileInfo);
        return Response::json([]);
    }
    
    public function uploadUpdate(Request $request, $id_diretorio, $id_arquivo) {
        if(!$request->file('file')) {
            $file = $this->fileRepository->info($id_arquivo);
            if(!$file) {
                App::abort(404);
            }
            $fileInfo = [
                'id_arquivo' => $id_arquivo,
                'id_diretorio' => $id_diretorio,
                'titulo' => $request->get('titulo') ? $request->get('titulo') : $tmp_nome,
                'descricao' => $request->get('descricao'),
                'credito' => $request->get('credito'),
                'arquivo_nome' => $file->arquivo_nome,
                'arquivo_tamanho' => $file->arquivo_tamanho,
                'arquivo_mime_type' => $file->arquivo_mime_type,
                'arquivo_extensao' => $file->arquivo_extensao,
                'arquivo_hash' => $file->arquivo_hash,
                'publicado' => $request->get('publicado', 'S')
            ];
        } else {
            $fileInfo = $this->uploadProcessa($request, $id_diretorio, $id_arquivo);
        }
        $this->fileRepository->update($fileInfo);
        return Response::json([]);
    }
    
    private function uploadProcessa(Request $request, $id_diretorio, $id_arquivo = null) {
        $output = null;
        $output_antigo = null;
        $hash = null;
        $hash_antigo = null;
        $tmp_nome = null;
        $storage = Storage::drive(env('WEBDISCO_STORAGE_DRIVER'));
        
        if($id_arquivo) {
            $file = $this->fileRepository->info($id_arquivo);
            if(!$file) {
                App::abort(404);
            }
            list($dia, $mes, $ano) = explode("/", UtilHelper::formatDate($file->created_at));
            $hash_antigo = $file->arquivo_hash;
            $tmp_extensao = $request->file('file')->getClientOriginalExtension();
            $tmp_nome = str_replace("." . $tmp_extensao, '', $request->file('file')->getClientOriginalName());
            $nome = UtilHelper::friendly($tmp_nome);
            $extensao = strtolower($tmp_extensao);
            $hash = $nome . "." . $extensao;
            
            if($extensao == 'jpg' || $extensao == 'jpeg') {
                $base_output = sprintf("/webdisco/%04d/%02d/jpg/original/", $ano, $mes);
            } else {
                $base_output = sprintf("/webdisco/%04d/%02d/outros/", $ano, $mes);
            }
            
            $output = $base_output . $nome . "." . $extensao;
            $output_antigo = $base_output . $hash_antigo;
            
            $hash = $nome . "." . $extensao;
            
            $i = 1;
            while($storage->exists($output )) {
                $output = $base_output . $nome . '-' . $i . '.' . $extensao;
                $hash = $nome . '-' . $i++ . "." . $extensao;
            }
        } else {
            $ano = date('Y');
            $mes = date('m');
            $tmp_extensao = $request->file('file')->getClientOriginalExtension();
            $tmp_nome = str_replace("." . $tmp_extensao, '', $request->file('file')->getClientOriginalName());
            $nome = UtilHelper::friendly($tmp_nome);
            $extensao = strtolower($tmp_extensao);

            if($extensao == 'jpg' || $extensao == 'jpeg') {
                $base_output = sprintf("/webdisco/%04d/%02d/jpg/original/", $ano, $mes);
            } else {
                $base_output = sprintf("/webdisco/%04d/%02d/outros/", $ano, $mes);
            }
            $output = $base_output . $nome . "." . $extensao;

            $hash = $nome . "." . $extensao;
            $i = 1;
            while($storage->exists($output )) {
                $output = $base_output . $nome . '-' . $i . '.' . $extensao;
                $hash = $nome . '-' . $i++ . "." . $extensao;
            }
        }
        
        if($hash_antigo) {
            $storage->delete($output_antigo);
        }
        
        $storage->put($output, file_get_contents($request->file('file')->getRealPath()));
        
        $fileInfo = [
            'id_arquivo' => $id_arquivo ? $id_arquivo : null,
            'id_diretorio' => $id_diretorio,
            'titulo' => $request->get('titulo') ? $request->get('titulo') : $tmp_nome,
            'descricao' => $request->get('descricao'),
            'credito' => $request->get('credito'),
            'arquivo_nome' => $request->file('file')->getClientOriginalName(),
            'arquivo_tamanho' => $request->file('file')->getClientSize(),
            'arquivo_mime_type' => $request->file('file')->getClientMimeType(),
            'arquivo_extensao' => $extensao,
            'arquivo_hash' => $hash,
            'publicado' => $request->get('publicado', 'S')
        ];
        return $fileInfo;
    }
    
    public function getImage(Request $request, $ano, $mes, $tamanho, $id_arquivo, $hash) {
        $path = sprintf("%s/webdisco/%04d/%02d/jpg/original/%s", storage_path('app'), $ano, $mes, $hash);
        $output = sprintf("%s/webdisco/%04d/%02d/jpg/%s/%d/%s", storage_path('app'), $ano, $mes,$tamanho, $id_arquivo,$hash);
        $outputPath = sprintf("%s/webdisco/%04d/%02d/jpg/%s/%d", storage_path('app'), $ano, $mes,$tamanho, $id_arquivo);
        
        if(strpos($tamanho, 'x') === false) {
            
            App::abort(404);
        }
        
        if(!file_exists($path)) {
            App::abort(404);
        }
        if(!is_dir($outputPath)) {
            mkdir($outputPath, 0777, true);
        }
        
        list($w, $h) = explode("x", $tamanho);
        Image::make($path)->resize($w, null, function($constraint) {
            $constraint->aspectRatio();
        })->save($output);
        return Image::make($output)->response('jpg');
    }
    
    public function getImageOriginal(Request $request, $ano, $mes, $hash) {
        $path = sprintf("%s/webdisco/%04d/%02d/jpg/original/%s", storage_path('app'), $ano, $mes, $hash);
        
        if(!file_exists($path)) {
            App::abort(404);
        }

        return Image::make($path)->response('jpg');
    }
    
    public function configureImagem(Request $request, $id_arquivo) {
        $imagem = $this->fileRepository->info($id_arquivo);
        return view('webdisco.embed.configure_imagem', [
            'imagem' => $imagem,
            'path' => StorageHelper::filePath($imagem, true)
        ]);
    }
    
    public function saveImagem(Request $request, $id_arquivo) {
        $imagem = $this->fileRepository->info($id_arquivo);
        $source = StorageHelper::filePath($imagem, false);
        $cropData = $request->get('cropData');
        $data = $request->get('data');
        $dt = explode('/', UtilHelper::formatDate($imagem->created_at));
        $outputDir = sprintf("%s/webdisco/%04d/%02d/jpg/custom/%d", storage_path('app'),$dt[2], $dt[1], $id_arquivo);
        $outputName = md5(time()) . '-'. round($cropData['width']) . 'x' . round($cropData['height']) . '.jpg';
        $outputUrl = sprintf("%s/%04d/%02d/jpg/custom/%d/%s", env('WEBDISCO_BASE_URL'), $dt[2], $dt[1], $id_arquivo, $outputName);
        
        if(!is_dir($outputDir)) {
            mkdir($outputDir, 0777, true);
        }
        if($data['rotate'] != 0) {
            $resource = Image::make(storage_path($source))->rotate(($data['rotate'] * -1));
        } else {
            $resource = Image::make(storage_path($source));
        }
        
        $resource->crop(round($data['width']), round($data['height']), round($data['x']), round($data['y']))
                ->resize(round($cropData['width']), round($cropData['height']), function($constraint){$constraint->aspectRatio();})
                ->save($outputDir . '/' . $outputName, 80);
        
        return Response::json([
            'imagem' => $outputUrl,
            'legenda' => $request->get('legenda'),
            'credito' => $request->get('credito'),
            'alinhamento' => $request->get('alinhamento')
        ]);
    }
}
