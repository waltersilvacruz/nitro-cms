<?php

Route::group([], function() {
    Route::get('/login',    ['as' => 'login', 'uses' => 'Auth\AuthController@index']);
    Route::post('/login',   ['as' => 'autentica', 'uses' => 'Auth\AuthController@authenticate']);
    Route::get('/logout',   ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
    Route::get('/ping', ['as' => 'ping', 'uses' => 'Auth\AuthController@ping']);

    Route::get('/',    ['as' => 'home', 'uses' => 'Dashboard\DashboardController@index', 'middleware' => ['auth']]);

    Route::group(['prefix' => 'webdisco', 'middleware' => ['auth']], function () {
        Route::get('/', ['as' => 'webdisco', 'uses' => 'Webdisco\WebdiscoController@index']);
        Route::get('/treeview', ['as' => 'webdisco.treeview', 'uses' => 'Webdisco\WebdiscoController@treeview']);
        Route::post('/treeview/create', ['as' => 'webdisco.treeview.create', 'uses' => 'Webdisco\WebdiscoController@create']);
        Route::post('/treeview/update', ['as' => 'webdisco.treeview.update', 'uses' => 'Webdisco\WebdiscoController@update']);
        Route::post('/treeview/destroy', ['as' => 'webdisco.treeview.destroy', 'uses' => 'Webdisco\WebdiscoController@destroy']);
        Route::get('/download/{id_arquivo}', ['as' => 'webdisco.file.download', 'uses' => 'Webdisco\WebdiscoController@download']);
        Route::get('/folder/{id_diretorio}/files', ['as' => 'webdisco.files', 'uses' => 'Webdisco\WebdiscoController@files']);
        Route::get('/folder/{id_diretorio}/upload', ['as' => 'webdisco.files.upload', 'uses' => 'Webdisco\WebdiscoController@upload']);
        Route::post('/folder/{id_diretorio}/upload', ['as' => 'webdisco.files.upload.create', 'uses' => 'Webdisco\WebdiscoController@uploadCreate']);
        Route::post('/folder/{id_diretorio}/upload/{id_arquivo}', ['as' => 'webdisco.files.upload.update', 'uses' => 'Webdisco\WebdiscoController@uploadUpdate']);
        Route::post('/folder/{id_diretorio}/file/{id_arquivo}/info', ['as' => 'webdisco.file.info', 'uses' => 'Webdisco\WebdiscoController@fileinfo']);
        Route::delete('/file', ['as' => 'webdisco.file.destroy', 'uses' => 'Webdisco\WebdiscoController@fileDestroy']);
        Route::get('/file/{id_arquivo}', ['as' => 'webdisco.file.get', 'uses' => 'Webdisco\WebdiscoController@file']);
        Route::post('/search', ['as' => 'webdisco.search', 'uses' => 'Webdisco\WebdiscoController@search']);
        Route::get('/embed/configureImagem/{id_arquivo}', ['as' => 'webdisco.embed.configureImagem', 'uses' => 'Webdisco\WebdiscoController@configureImagem']);
        Route::post('/embed/configureImagem/{id_arquivo}', ['as' => 'webdisco.embed.configureImagem.save', 'uses' => 'Webdisco\WebdiscoController@saveImagem']);
    });

    Route::group(['prefix' => 'storage'], function () {
        Route::get('/webdisco/{ano}/{mes}/jpg/{tamanho}/{id_arquivo}/{hash}', ['as' => 'webdisco.image', 'uses' => 'Webdisco\WebdiscoController@getImage']);
        Route::get('/webdisco/{ano}/{mes}/jpg/original/{hash}', ['as' => 'webdisco.image', 'uses' => 'Webdisco\WebdiscoController@getImageOriginal']);
    });

    Route::group(['prefix' => 'menu', 'middleware' => ['auth']], function(){
        Route::get('/', ['as' => 'menu', 'uses' => 'Menu\MenuController@index']);
        Route::get('/create', ['as' => 'menu.novo', 'uses' => 'Menu\MenuController@novo']);
        Route::post('/create', ['as' => 'menu.create', 'uses' => 'Menu\MenuController@create']);
        Route::get('/{id_menu}', ['as' => 'menu.edita', 'uses' => 'Menu\MenuController@edita']);
        Route::put('/{id_menu}', ['as' => 'menu.update', 'uses' => 'Menu\MenuController@update']);
        Route::delete('/{id_menu}', ['as' => 'menu.destroy', 'uses' => 'Menu\MenuController@destroy']);

        Route::get('/{id_menu}/pagina', ['as' => 'menu.pagina', 'uses' => 'Menu\PaginaController@index']);
        Route::get('/{id_menu}/pagina/treeview', ['as' => 'menu.pagina.treeview', 'uses' => 'Menu\PaginaController@treeview']);
        Route::post('/{id_menu}/pagina', ['as' => 'menu.pagina.create', 'uses' => 'Menu\PaginaController@create']);
        Route::get('/{id_menu}/pagina/{id_pagina}', ['as' => 'menu.pagina.edit', 'uses' => 'Menu\PaginaController@edit']);
        Route::put('/{id_menu}/pagina/{id_pagina}', ['as' => 'menu.pagina.update', 'uses' => 'Menu\PaginaController@update']);
        Route::put('/{id_menu}/pagina/{id_pagina}/rename', ['as' => 'menu.pagina.rename', 'uses' => 'Menu\PaginaController@rename']);
        Route::delete('/{id_menu}/pagina/{id_pagina}', ['as' => 'menu.pagina.destroy', 'uses' => 'Menu\PaginaController@destroy']);
        Route::post('/{id_menu}/pagina/{id_pagina}/sort', ['as' => 'menu.pagina.sort', 'uses' => 'Menu\PaginaController@sort']);
    });

    Route::group(['prefix' => 'secao', 'middleware' => ['auth']], function(){
        Route::get('/', ['as' => 'secao', 'uses' => 'Secao\SecaoController@index']);
        Route::get('/create', ['as' => 'secao.novo', 'uses' => 'Secao\SecaoController@novo']);
        Route::post('/create', ['as' => 'secao.create', 'uses' => 'Secao\SecaoController@create']);
        Route::get('/{id_secao}', ['as' => 'secao.edita', 'uses' => 'Secao\SecaoController@edita']);
        Route::put('/{id_secao}', ['as' => 'secao.update', 'uses' => 'Secao\SecaoController@update']);
        Route::delete('/{id_secao}', ['as' => 'secao.destroy', 'uses' => 'Secao\SecaoController@destroy']);

        Route::get('/{id_secao}/materias', ['as' => 'secao.materias', 'uses' => 'Secao\MateriaController@paginate']);
        Route::get('/{id_secao}/materia', ['as' => 'secao.materia', 'uses' => 'Secao\MateriaController@index']);
        Route::get('/{id_secao}/materia/novo', ['as' => 'secao.materia.novo', 'uses' => 'Secao\MateriaController@novo']);
        Route::post('/{id_secao}/materia', ['as' => 'secao.materia.create', 'uses' => 'Secao\MateriaController@create']);
        Route::get('/{id_secao}/materia/{id_materia}', ['as' => 'secao.materia.edita', 'uses' => 'Secao\MateriaController@edit']);
        Route::put('/{id_secao}/materia/{id_materia}', ['as' => 'secao.materia.update', 'uses' => 'Secao\MateriaController@update']);
        Route::delete('/{id_secao}/materia/{id_materia}', ['as' => 'secao.materia.destroy', 'uses' => 'Secao\MateriaController@destroy']);
    });

    Route::group(['prefix' => 'util', 'middleware' => ['auth']], function(){
        Route::get('/autocomplete/tags', ['as' => 'util.tags', 'uses' => 'Util\UtilController@tags']);
        Route::get('/autocomplete/entidades', ['as' => 'util.tags', 'uses' => 'Util\UtilController@entidades']);
        Route::get('/randomImage/{w}/{h}', ['as' => 'util.randomImage', 'uses' => 'Util\UtilController@randomImage']);
    });
    
    Route::group(['prefix' => 'builder', 'middleware' => ['auth']], function(){
        Route::get('/', ['as' => 'builder.index', 'uses' => 'Builder\BuilderController@index']);
        Route::get('/editor/{id_template?}', ['as' => 'builder.editor', 'uses' => 'Builder\BuilderController@editor']);
        Route::post('/editor', ['as' => 'builder.create', 'uses' => 'Builder\BuilderController@create']);
        Route::put('/editor/{id_template}', ['as' => 'builder.update', 'uses' => 'Builder\BuilderController@update']);
        Route::get('/load/{id_template}', ['as' => 'builder.load', 'uses' => 'Builder\BuilderController@load']);
        Route::get('/layouts', ['as' => 'builder.layouts', 'uses' => 'Builder\BuilderController@layouts']);
        Route::delete('/editor/{id_template}', ['as' => 'builder.destroy', 'uses' => 'Builder\BuilderController@destroy']);
    });
    
    Route::group(['prefix' => 'artigo', 'middleware' => ['auth']], function(){
        Route::get('/', ['as' => 'artigo', 'uses' => 'Artigo\ArtigoController@index']);
        Route::get('/create', ['as' => 'artigo.novo', 'uses' => 'Artigo\ArtigoController@novo']);
        Route::post('/create', ['as' => 'artigo.create', 'uses' => 'Artigo\ArtigoController@create']);
        Route::get('/{id_artigo}', ['as' => 'artigo.edita', 'uses' => 'Artigo\ArtigoController@edita'])->where(['id_artigo' => '[0-9]+']);
        Route::put('/{id_artigo}', ['as' => 'artigo.update', 'uses' => 'Artigo\ArtigoController@update'])->where(['id_artigo' => '[0-9]+']);
        Route::delete('/{id_artigo}', ['as' => 'artigo.destroy', 'uses' => 'Artigo\ArtigoController@destroy'])->where(['id_artigo' => '[0-9]+']);

        Route::get('/articulista', ['as' => 'artigo.articulista', 'uses' => 'Artigo\ArticulistaController@index']);
        Route::get('/articulista/create', ['as' => 'artigo.articulista.novo', 'uses' => 'Artigo\ArticulistaController@novo']);
        Route::post('/articulista/create', ['as' => 'artigo.articulista.create', 'uses' => 'Artigo\ArticulistaController@create']);
        Route::get('/articulista/{id_articulista}', ['as' => 'artigo.articulista.edita', 'uses' => 'Artigo\ArticulistaController@edita']);
        Route::put('/articulista/{id_articulista}', ['as' => 'artigo.articulista.update', 'uses' => 'Artigo\ArticulistaController@update']);
        Route::delete('/articulista/{id_articulista}', ['as' => 'artigo.articulista.destroy', 'uses' => 'Artigo\ArticulistaController@destroy']);
    });
    
});
