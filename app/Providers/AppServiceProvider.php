<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Blade::directive('enabledIf', function($expression) {
            list($recurso, $acao) = explode(',',str_replace(['(',')',' ',"'",'"'], '', $expression));
            $temAcesso = 'true';
            return $temAcesso ? '' : 'disabled="disabled"';
        });
    }
}
