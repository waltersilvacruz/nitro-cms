<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\AuditingTrait;

class PaginaLinkModel extends Model {

    use AuditingTrait;

    protected $table = 'pagina_link';
    public $primaryKey = 'id_pagina_link';
    public $timestamps = false;
    public $incrementing = true;

    public function pagina() {
        return $this->hasOne('App\Databases\Models\PaginaModel', 'id_pagina', 'id_pagina');
    }
}
