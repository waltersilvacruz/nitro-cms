<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VwPaginaModel extends Model {
    
    use SoftDeletes;
    
    protected $table = 'vw_pagina';
    public $primaryKey = 'id_pagina';
    public $timestamps = true;
    
    public function menu() {
        return $this->hasOne('App\Databases\Models\MenuModel', 'id_menu', 'id_menu');
    }
}
