<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;

class TagModel extends Model {

    protected $table = 'tag';
    public $primaryKey = 'tag';
    public $timestamps = false;
    public $incrementing = false;

}
