<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class MateriaAnexoModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'materia_anexo';
    public $primaryKey = 'id_materia_anexo';
    public $timestamps = true;
    public $incrementing = true;

    public function materia() {
        return $this->hasOne('App\Databases\Models\MateriaModel', 'id_materia', 'id_materia');
    }

    public function arquivo() {
        return $this->hasOne('App\Databases\Models\ArquivoModel', 'id_arquivo', 'id_arquivo');
    }
}
