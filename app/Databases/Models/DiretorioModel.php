<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class DiretorioModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'diretorio';
    public $primaryKey = 'id_diretorio';
    public $timestamps = true;
    public $incrementing = true;

    public function arquivos() {
        return $this->hasMany('App\Databases\Models\ArquivoModel', 'id_diretorio', 'id_diretorio');
    }
}
