<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class ArtigoModel extends Model {
    
    use SoftDeletes;
    use AuditingTrait;
    
    protected $table = 'artigo';
    public $primaryKey = 'id_artigo';
    public $sequence = 'seq_artigo';
    public $timestamps = true;
    
    public function articulista() {
        return $this->belongsTo('App\Databases\Models\ArticulistaModel', 'id_articulista', 'id_articulista');
    }
    
    public function anexos() {
        return $this->hasMany('App\Databases\Models\ArtigoAnexoModel', 'id_artigo', 'id_artigo');
    }
}
