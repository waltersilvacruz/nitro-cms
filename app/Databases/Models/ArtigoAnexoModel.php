<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class ArtigoAnexoModel extends Model {
    
    use SoftDeletes;
    use AuditingTrait;
    
    protected $table = 'artigo_anexo';
    public $primaryKey = 'id_artigo_anexo';
    public $timestamps = true;
    public $sequence = 'seq_artigo_anexo';
    
    public function artigo() {
        return $this->hasOne('App\Databases\Models\ArtigoModel', 'id_artigo', 'id_artigo');
    }
    
    public function arquivo() {
        return $this->hasOne('App\Databases\Models\ArquivoModel', 'id_arquivo', 'id_arquivo');
    }    
}
