<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;

class TagConteudoModel extends Model {

    protected $table = 'tag_conteudo';
    public $primaryKey = 'tag';
    public $timestamps = false;
    public $incrementing = false;
}
