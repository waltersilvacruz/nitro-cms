<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class MenuModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'menu';
    public $primaryKey = 'id_menu';
    public $timestamps = true;
    public $incrementing = true;

    public function paginas() {
        return $this->hasMany('App\Databases\Models\PaginaModel', 'id_menu', 'id_menu');
    }
}
