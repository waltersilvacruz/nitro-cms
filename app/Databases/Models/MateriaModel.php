<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class MateriaModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'materia';
    public $primaryKey = 'id_materia';
    public $timestamps = true;
    public $incrementing = true;

    public function secao() {
        return $this->hasOne('App\Databases\Models\SecaoModel', 'id_secao', 'id_secao');
    }
    
    public function usuario() {
        return $this->hasOne('App\Databases\Models\UsuarioModel', 'id_usuario', 'created_by');
    }
}
