<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class ArquivoModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'arquivo';
    public $primaryKey = 'id_arquivo';
    public $timestamps = true;
    public $incrementing = true;

    public function diretorio() {
        return $this->belongsTo('App\Databases\Models\DiretorioModel', 'id_diretorio', 'id_diretorio');
    }
}
