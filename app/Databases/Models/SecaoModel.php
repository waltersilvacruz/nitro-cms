<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class SecaoModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'secao';
    public $primaryKey = 'id_secao';
    public $timestamps = true;
    public $incrementing = true;

    public function materias() {
        return $this->hasMany('App\Databases\Models\MateriaModel', 'id_secao', 'id_secao');
    }
}
