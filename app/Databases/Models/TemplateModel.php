<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class TemplateModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'template';
    public $primaryKey = 'id_template';
    public $timestamps = true;
    public $incrementing = true;

}
