<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class PaginaModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'pagina';
    public $primaryKey = 'id_pagina';
    public $timestamps = true;
    public $incrementing = true;

    public function menu() {
        return $this->hasOne('App\Databases\Models\MenuModel', 'id_menu', 'id_menu');
    }

    public function anexos() {
        return $this->hasMany('App\Databases\Models\PaginaAnexoModel', 'id_pagina', 'id_pagina');
    }
}
