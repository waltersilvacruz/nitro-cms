<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class ArticulistaModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'articulista';
    public $primaryKey = 'id_articulista';
    public $timestamps = true;
    public $incrementing = true;

    public function artigos() {
        return $this->hasMany('App\Databases\Models\ArtigoModel', 'id_articulista', 'id_articulista');
    }    
    
    public function foto() {
        return $this->belongsTo('App\Databases\Models\ArquivoModel', 'id_arquivo', 'id_arquivo');
    }
}
