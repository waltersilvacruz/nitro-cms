<?php

namespace App\Databases\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class PaginaAnexoModel extends Model {

    use SoftDeletes;
    use AuditingTrait;

    protected $table = 'pagina_anexo';
    public $primaryKey = 'id_pagina_anexo';
    public $timestamps = true;
    public $incrementing = true;

    public function pagina() {
        return $this->hasOne('App\Databases\Models\PaginaModel', 'id_pagina', 'id_pagina');
    }

    public function arquivo() {
        return $this->hasOne('App\Databases\Models\ArquivoModel', 'id_arquivo', 'id_arquivo');
    }
}
