<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\VwPaginaModel;
use App\Helpers\UtilHelper;
use DB;
use Auth;

class VwPaginaRepository {

    private $model;

    public function __construct(VwPaginaModel $model) {
        $this->model = $model;
    }
}