<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\ArquivoModel;
use App\Helpers\StorageHelper;
use App\Helpers\UtilHelper;
use DB;
use Auth;

class ArquivoRepository {

    private $model;

    public function __construct(ArquivoModel $model) {
        $this->model = $model;
    }

    public function listFiles($source, $filtro) {
        $id_diretorio = null;
        if(!is_array($source)) {
            $id_diretorio = $source;
        }

        $ret = [];
        $arrFiltros = [
            'imagem' => ['jpg'],
            'inline_image' => ['jpg'],
            'video' => ['mp4']
        ];
        if($filtro == 'imagem' || $filtro == 'inline_image') {
            $arquivos = $this->model->where('id_diretorio', $id_diretorio)->whereIn('arquivo_extensao', $arrFiltros[$filtro])->orderBy("titulo")->get();
        } else {
            if(is_array($source)) {
                $arquivos = $this->model->whereIn('id_arquivo', $source)->orderBy("titulo")->get();
            } else {
                $arquivos = $this->model->where('id_diretorio', $id_diretorio)->orderBy("titulo")->get();
            }

        }
        foreach($arquivos as $arquivo) {
            $path = StorageHelper::filePath($arquivo, true);
            if($arquivo->arquivo_extensao === 'jpg' || $arquivo->arquivo_extensao === 'jpeg') {
                $thumbnail = str_replace("/original/", "/190x140/" . $arquivo->id_arquivo . "/", $path) . '?ts=' . time();
                $thumbnail_small = str_replace("/original/", "/64x48/" . $arquivo->id_arquivo . "/", $path) . '?ts=' . time();
            } else {
                $thumbnail = null;
                $thumbnail_small = null;
            }
            $arquivo->data_formatada = UtilHelper::formatDate($arquivo->created_at, true);
            $arquivo->arquivo_tamanho = UtilHelper::filesize($arquivo->arquivo_tamanho);
            $arquivo->path = $path;
            $arquivo->thumbnail = $thumbnail;
            $arquivo->thumbnail_small = $thumbnail_small;
            array_push($ret, $arquivo);
        }
        return $ret;
    }

    public function searchFiles($keyword) {
        $ret = [];
        $arquivos = $this->model->where('upper(titulo)', 'like', '%' . mb_strtoupper($keyword) . '%')->orderBy("titulo")->get();
        foreach($arquivos as $arquivo) {
            $path = StorageHelper::filePath($arquivo, true);
            if($arquivo->arquivo_extensao === 'jpg' || $arquivo->arquivo_extensao === 'jpeg') {
                $thumbnail = str_replace("/original/", "/190x140/" . $arquivo->id_arquivo . "/", $path) . '?ts=' . time();
                $thumbnail_small = str_replace("/original/", "/64x48/" . $arquivo->id_arquivo . "/", $path) . '?ts=' . time();
            } else {
                $thumbnail = null;
                $thumbnail_small = null;
            }
            $arquivo->data_formatada = UtilHelper::formatDate($arquivo->created_at, true);
            $arquivo->arquivo_tamanho = UtilHelper::filesize($arquivo->arquivo_tamanho);
            $arquivo->path = $path;
            $arquivo->thumbnail = $thumbnail;
            $arquivo->thumbnail_small = $thumbnail_small;
            array_push($ret, $arquivo);
        }
        return $ret;
    }

    public function info($id_arquivo) {
        $arquivo = DB::table('vw_arquivo')->where('id_arquivo', $id_arquivo)->first();
        return $arquivo;
    }

    public function create($input) {
        DB::beginTransaction();
        try {
            $arquivo = $this->model;
            $arquivo->id_diretorio = $input['id_diretorio'];
            $arquivo->titulo = $input['titulo'];
            $arquivo->descricao = $input['descricao'];
            $arquivo->credito = $input['credito'];
            $arquivo->arquivo_nome = $input['arquivo_nome'];
            $arquivo->arquivo_extensao = $input['arquivo_extensao'];
            $arquivo->arquivo_tamanho = $input['arquivo_tamanho'];
            $arquivo->arquivo_mime_type = $input['arquivo_mime_type'];
            $arquivo->arquivo_hash = $input['arquivo_hash'];
            $arquivo->publicado = $input['publicado'];
            $arquivo->created_by = Auth::user()->id_usuario;
            $arquivo->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($input) {
        DB::beginTransaction();
        try {
            $arquivo = $this->model->where('id_arquivo', $input['id_arquivo'])->first();
            $arquivo->id_diretorio = $input['id_diretorio'];
            $arquivo->titulo = $input['titulo'];
            $arquivo->descricao = $input['descricao'];
            $arquivo->credito = $input['credito'];
            $arquivo->arquivo_nome = $input['arquivo_nome'];
            $arquivo->arquivo_extensao = $input['arquivo_extensao'];
            $arquivo->arquivo_tamanho = $input['arquivo_tamanho'];
            $arquivo->arquivo_mime_type = $input['arquivo_mime_type'];
            $arquivo->arquivo_hash = $input['arquivo_hash'];
            $arquivo->publicado = $input['publicado'];
            $arquivo->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($input) {
        $files = explode(",", $input['files']);
        return $this->model->whereIn('id_arquivo', $files)->delete();
    }
}
