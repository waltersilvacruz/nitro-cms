<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\MateriaAnexoModel;

class MateriaAnexoRepository {

    private $model;

    public function __construct(MateriaAnexoModel $model) {
        $this->model = $model;
    }
    
    public function getAll($id_materia) {
        return $this->model->where('id_materia', $id_materia)->get();
    }
    
    public function destroyAll($id_materia) {
        return $this->model->where('id_materia', $id_materia)->delete();
    }
}