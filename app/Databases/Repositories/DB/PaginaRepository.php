<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\PaginaModel;
use App\Databases\Models\VwPaginaModel;
use App\Databases\Models\PaginaAnexoModel;
use App\Databases\Models\PaginaLinkModel;
use App\Helpers\UtilHelper;
use DB;
use Auth;
use Exception;

class PaginaRepository {

    private $model;

    public function __construct(PaginaModel $model) {
        $this->model = $model;
    }

    public function get($id_menu, $id_pagina) {
        $pagina = VwPaginaModel::where('id_menu', $id_menu)->where('id_pagina', $id_pagina)->first();
        return $pagina;
    }

    public function getAnexos($id_pagina) {
        $anexos = PaginaAnexoModel::where('id_pagina', $id_pagina)->get();
        return $anexos;
    }
    
    public function create($data, $id_menu) {
        DB::beginTransaction();
        try {
            $pagina = $this->model;

            // pegamos a ordem
            $ordem = 1;
            $pagina_ordem = $this->model->where('id_pagina_superior', $data['parent'])->orderBy('ordem', 'desc')->first();
            if ($pagina_ordem) {
                $ordem = $pagina_ordem->ordem + 1;
            }

            // slugify
            $slug = UtilHelper::slugify($this->model, $data['titulo']);
            $pagina->id_menu = $id_menu;
            $pagina->id_pagina_superior = $data['parent'];
            $pagina->titulo = $data['titulo'];
            $pagina->slug = $slug;
            $pagina->publicado = 'N';
            $pagina->tipo_pagina = '1';
            $pagina->ordem = $ordem;
            $pagina->created_by = Auth::user()->id_usuario;
            $pagina->save();

            // verifica se existem anexos
            if (isset($data['anexos'])) {
                foreach (explode(",", $data['anexos']) as $id_arquivo) {
                    $anexo = new PaginaAnexoModel();
                    $anexo->id_pagina = $pagina->id_pagina;
                    $anexo->id_arquivo = $id_arquivo;
                    $anexo->save();
                }
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }

        return $pagina;
    }

    public function update($input, $id_menu, $id_pagina) {
        DB::beginTransaction();

        try {
            $pagina = $this->model->where('id_menu', $id_menu)->where('id_pagina', $id_pagina)->first();
            if (!$pagina) {
                abort(404);
            }

            if ($pagina->titulo == $input['titulo']) {
                $slug = UtilHelper::slugify($this->model, $input['titulo']);
                $pagina->slug = $slug;
            }
            $pagina->titulo = $input['titulo'];
            $pagina->tipo_pagina = $input['tipo_pagina'];
            $pagina->publicado = isset($input['publicado']) ? $input['publicado'] : 'N';

            if ($input['tipo_pagina'] == 1) {
                $pagina->texto = $input['texto'];
            } else if ($input['tipo_pagina'] == 3) {
                $pagina->redirect = $input['redirect'];
                $pagina->redirect_target = $input['redirect_target'];
            }
            $pagina->save();

            // verifica se existem anexos
            if (isset($input['anexos']) && strlen($input['anexos']) > 0) {
                foreach (explode(",", $input['anexos']) as $id_arquivo) {
                    $pagina_anexo = PaginaAnexoModel::where('id_pagina', $pagina->id_pagina)->where('id_arquivo', $id_arquivo)->first();
                    if (!$pagina_anexo) {
                        $anexo = new PaginaAnexoModel();
                        $anexo->id_pagina = $pagina->id_pagina;
                        $anexo->id_arquivo = $id_arquivo;
                        $anexo->created_by = Auth::user()->id_usuario;
                        $anexo->save();
                    }
                }

                PaginaAnexoModel::whereNotIn('id_arquivo', explode(",", $input['anexos']))->where('id_pagina', $pagina->id_pagina)->delete();
            } else {
                PaginaAnexoModel::where('id_pagina', $pagina->id_pagina)->delete();
            }

            DB::commit();
            return $pagina;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function rename($input, $id_menu, $id_pagina) {
        DB::beginTransaction();

        try {
            $pagina = $this->model->where('id_menu', $id_menu)->where('id_pagina', $id_pagina)->first();
            if (!$pagina) {
                abort(404);
            }

            if ($pagina->titulo != $input['titulo']) {
                $slug = UtilHelper::slugify($this->model, $input['titulo']);
                $pagina->slug = $slug;
            }
            $pagina->titulo = $input['titulo'];
            $pagina->save();
            DB::commit();
            return $pagina;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($input, $id_menu, $id_pagina) {
        DB::beginTransaction();
        try {
            $pagina = $this->model->where('id_menu', $id_menu)->where('id_pagina', $id_pagina)->first();
            if (!$pagina) {
                abort(404);
            }
            $pagina->delete();
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function sort($input, $id_menu, $id_pagina_superior) {
        if ($id_pagina_superior < 0) {
            $id_pagina_superior = 0;
        }
        DB::beginTransaction();
        try {
            $items = explode(',', $input['items']);
            for ($i = 0; $i < count($items); $i++) {
                $pagina = $this->model->where('id_menu', $id_menu)->where('id_pagina', $items[$i])->first();
                if (!$pagina) {
                    abort(404);
                }
                $pagina->id_pagina_superior = $id_pagina_superior;
                $pagina->ordem = ($i + 1);
                $pagina->save();
            }
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

}
