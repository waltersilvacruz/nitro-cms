<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\TagModel;
use DB;
use Auth;
use Exception;

class TagRepository {

    private $model;

    
    public function search($input) {
        $ret = [];
        $itens = TagModel::where('tag', 'like', '%' . strtolower($input['q']) . '%')->orderBy("tag")->take(10)->get();
        foreach($itens as $item) {
            array_push($ret, $item->tag);
        }
        return $ret;
    }
    
}
