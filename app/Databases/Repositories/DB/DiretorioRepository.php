<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\DiretorioModel;
use DB;
use Auth;

class DiretorioRepository {

    private $model;

    public function __construct(DiretorioModel $model) {
        $this->model = $model;
    }

    public function create($data) {
        DB::beginTransaction();
        try {
            $diretorio = $this->model;
            $diretorio->id_diretorio_superior = $data['parent'];
            $diretorio->nome = $data['text'];
            $diretorio->publicado = 'S';
            $diretorio->created_by = Auth::user()->id_usuario;
            $diretorio->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }

        return $diretorio->id_diretorio;
    }

    public function update($data) {
        $id = $data['id'];
        $text = $data['text'];
        DB::beginTransaction();
        try {
            $diretorio = $this->model->where('id_diretorio', $id)->first();
            if(!$diretorio) {
                App::abort(404);
            }
            $diretorio->nome = $text;
            $diretorio->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data) {
        $id = $data['id'];
        DB::beginTransaction();
        try {
            $diretorio = $this->model->where('id_diretorio', $id)->first();
            if(!$diretorio) {
                App::abort(404);
            }
            $diretorio->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }
}
