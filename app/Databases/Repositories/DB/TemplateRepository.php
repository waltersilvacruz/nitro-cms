<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\TemplateModel;
use App\Helpers\UtilHelper;
use DB;
use Auth;
use Exception;

class TemplateRepository {

    private $model;

    public function __construct(TemplateModel $model) {
        $this->model = $model;
    }

    public function getAll() {
        return $this->model->orderBy("created_at")->get();
    }

    public function getById($id_template) {
        return $this->model->where('id_template', $id_template)->first();
    }

    public function create($input) {
        DB::beginTransaction();
        try {
            $template = new TemplateModel();
            $template->nome = $input['nome'];
            $template->descricao = $input['descricao'];
            $template->thumbnail = $input['thumbnail'];
            $template->configuracao = $input['configuracao'];
            $template->publicado= isset($input['publicado']) ? $input['publicado'] : 'N';
            $template->created_by = Auth::user()->id_usuario;
            $template->save();
            DB::commit();
            return $template;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($id_template, $input) {
        DB::beginTransaction();
        try {
            $template = $this->model->where('id_template',$id_template)->first();
            if(!$template) {
                abort(404);
            }
            $template->nome = $input['nome'];
            $template->descricao = $input['descricao'];
            $template->thumbnail = $input['thumbnail'];
            $template->configuracao = $input['configuracao'];
            $template->publicado= isset($input['publicado']) ? $input['publicado'] : 'N';
            $template->save();
            DB::commit();
            return $template;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($id_template) {
        DB::beginTransaction();
        try {
            $template = $this->model->where('id_template',$id_template)->first();
            if(!$template) {
                abort(404);
            }
            $template->delete();
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }
}
