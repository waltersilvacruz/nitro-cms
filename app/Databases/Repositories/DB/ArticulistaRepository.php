<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\ArticulistaModel;
use App\Helpers\UtilHelper;
use DB;
use Auth;
use Exception;

class ArticulistaRepository {

    private $model;

    public function __construct(ArticulistaModel $model) {
        $this->model = $model;
    }

    public function getAll() {
        return $this->model->orderBy("nome")->get();
    }

    public function getById($id_articulista) {
        return $this->model->where('id_articulista', $id_articulista)->first();
    }

    public function create($input) {
        DB::beginTransaction();
        try {
            // slugify
            $slug = UtilHelper::slugify($this->model, $input['nome']);
            $articulista = new ArticulistaModel();
            $articulista->id_arquivo = $input['foto'];
            $articulista->nome = $input['nome'];
            $articulista->slug = $slug;
            $articulista->resumo = $input['resumo'];
            $articulista->publicado = isset($input['publicado']) ? $input['publicado'] : 'N';
            $articulista->created_by = Auth::user()->id_usuario;
            $articulista->save();
            DB::commit();
            return $articulista;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($id_articulista, $input) {
        DB::beginTransaction();
        try {
            $articulista = $this->model->where('id_articulista', $id_articulista)->first();
            if (!$articulista) {
                abort(404);
            }
            if ($articulista->nome != $input['nome']) {
                // slugify
                $slug = UtilHelper::slugify($this->model, $input['nome']);
                $articulista->slug = $slug;
            }
            $articulista->id_arquivo = $input['foto'];
            $articulista->nome = $input['nome'];
            $articulista->resumo = $input['resumo'];
            $articulista->publicado = isset($input['publicado']) ? $input['publicado'] : 'N';
            $articulista->save();
            DB::commit();
            return $articulista;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($id_articulista) {
        DB::beginTransaction();
        try {
            $articulista = $this->model->where('id_articulista', $id_articulista)->first();
            if (!$articulista) {
                abort(404);
            }
            $articulista->delete();
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

}
