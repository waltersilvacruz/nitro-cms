<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\PaginaAnexoModel;

class PaginaAnexoRepository {

    private $model;

    public function __construct(PaginaAnexoModel $model) {
        $this->model = $model;
    }
    
    public function getAll($id_pagina) {
        return $this->model->where('id_pagina', $id_pagina)->get();
    }
    
    public function destroyAll($id_pagina) {
        return $this->model->where('id_pagina', $id_pagina)->delete();
    }
}