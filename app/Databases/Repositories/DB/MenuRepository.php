<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\MenuModel;
use App\Helpers\UtilHelper;
use DB;
use Auth;
use Exception;

class MenuRepository {

    private $model;

    public function __construct(MenuModel $model) {
        $this->model = $model;
    }

    public function getAll() {
        return $this->model->orderBy("nome")->get();
    }

    public function getById($id_menu) {
        return $this->model->where('id_menu', $id_menu)->first();
    }

    public function create($input) {
        DB::beginTransaction();
        try {
            $menu = new MenuModel();
            $menu->tipo = $input['tipo'];
            $menu->nome = $input['nome'];
            $menu->descricao = $input['descricao'];
            $menu->publicado= isset($input['publicado']) ? $input['publicado'] : 'N';
            $menu->publico = isset($input['publico']) ? $input['publico'] : 'N';
            $menu->created_by = Auth::user()->id_usuario;
            $menu->save();
            DB::commit();
            return $menu;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($id_menu, $input) {
        DB::beginTransaction();
        try {
            $menu = $this->model->where('id_menu',$id_menu)->first();
            if(!$menu) {
                abort(404);
            }
            $menu->tipo = $input['tipo'];
            $menu->nome = $input['nome'];
            $menu->descricao = $input['descricao'];
            $menu->publicado= isset($input['publicado']) ? $input['publicado'] : 'N';
            $menu->publico = isset($input['publico']) ? $input['publico'] : 'N';
            $menu->save();
            DB::commit();
            return $menu;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($id_menu) {
        DB::beginTransaction();
        try {
            $menu = $this->model->where('id_menu',$id_menu)->first();
            if(!$menu) {
                abort(404);
            }
            $menu->delete();
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }
}
