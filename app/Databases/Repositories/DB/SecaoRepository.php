<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\SecaoModel;
use App\Helpers\UtilHelper;
use DB;
use Auth;
use Exception;

class SecaoRepository {

    private $model;

    public function __construct(SecaoModel $model) {
        $this->model = $model;
    }

    public function getAll() {
        return $this->model->orderBy("nome")->get();
    }

    public function getById($id_secao) {
        return $this->model->where('id_secao', $id_secao)->first();
    }

    public function create($input) {
        DB::beginTransaction();
        try {
            $slug = UtilHelper::slugify($this->model, $input['nome']);
            $secao = new SecaoModel();
            $secao->nome = $input['nome'];
            $secao->slug = $slug;
            $secao->descricao = $input['descricao'];
            $secao->publicado= isset($input['publicado']) ? $input['publicado'] : 'N';
            $secao->publico = isset($input['publico']) ? $input['publico'] : 'N';
            $secao->created_by = Auth::user()->id_usuario;
            $secao->save();
            DB::commit();
            return $secao;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($id_secao, $input) {
        DB::beginTransaction();
        try {
            $secao = $this->model->where('id_secao',$id_secao)->first();
            if(!$secao) {
                abort(404);
            }
            if($secao->nome != $input['nome']) {
                $slug = UtilHelper::slugify($this->model, $input['nome']);
                $secao->slug = $slug;
            }
            $secao->nome = $input['nome'];
            $secao->descricao = $input['descricao'];
            $secao->publicado= isset($input['publicado']) ? $input['publicado'] : 'N';
            $secao->publico = isset($input['publico']) ? $input['publico'] : 'N';
            $secao->save();
            DB::commit();
            return $secao;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($id_secao) {
        DB::beginTransaction();
        try {
            $secao = $this->model->where('id_secao',$id_secao)->first();
            if(!$secao) {
                abort(404);
            }
            $secao->delete();
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }
}
