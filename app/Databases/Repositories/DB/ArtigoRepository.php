<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\ArtigoModel;
use App\Databases\Models\ArtigoAnexoModel;
use App\Databases\Models\TagModel;
use App\Databases\Models\TagConteudoModel;
use App\Helpers\UtilHelper;
use DB;
use Auth;
use Exception;

class ArtigoRepository {

    private $model;

    public function __construct(ArtigoModel $model) {
        $this->model = $model;
    }

    public function getAll() {
        return $this->model->with('articulista')->orderBy("data_artigo")->get();
    }

    public function getById($id_artigo) {
        return $this->model->where('id_artigo', $id_artigo)->first();
    }
    
    public function getAnexos($id_artigo) {
        return ArtigoAnexoModel::where('id_artigo', $id_artigo)->get();
    }
    
    public function getTags($id_artigo) {
        return TagConteudoModel::where('conteudo', $id_artigo)->where('tipo', 'A')->orderBy('tag', 'asc')->get();
    }

    public function create($input) {
        DB::beginTransaction();
        try {
            // slugify
            $slug = UtilHelper::slugify($this->model, $input['titulo']);
            $artigo = new ArtigoModel();
            $artigo->id_articulista = $input['articulista'];
            $artigo->id_arquivo = $input['foto'] ? $input['foto'] : null;
            $artigo->data_artigo = UtilHelper::formatDate($input['data'] . ' ' . $input['hora'], true);
            $artigo->chapeu = $input['chapeu'];
            $artigo->titulo = $input['titulo'];
            $artigo->slug = $slug;
            $artigo->complemento = $input['complemento'];
            $artigo->texto = $input['texto'];
            $artigo->publicado = isset($input['publicado']) ? $input['publicado'] : 'N';
            $artigo->created_by = Auth::user()->id_usuario;
            $artigo->save();
            
            
            // verifica se existem anexos
            if ($input['anexos']) {
                foreach (explode(",", $input['anexos']) as $id_arquivo) {
                    $anexo = new ArtigoAnexoModel();
                    $anexo->id_artigo = $artigo->id_artigo;
                    $anexo->id_arquivo = $id_arquivo;
                    $anexo->created_by = Auth::user()->id_usuario;
                    $anexo->save();
                }
            }

            // verifica se tem tags
            if ($input['tags']) {
                foreach (explode(",", $input['tags']) as $mtag) {
                    // verifica se a tag existe
                    $tag = TagModel::where('tag', mb_strtolower(trim($mtag)))->first();
                    if (!$tag) {
                        $tag = new TagModel();
                        $slug = UtilHelper::slugify($tag, $mtag, false);
                        $tag->tag = mb_strtolower(trim($mtag));
                        $tag->slug = $slug;
                        $tag->save();
                    }
                    
                    $artigo_tag = new TagConteudoModel();
                    $artigo_tag->conteudo = $artigo->id_artigo;
                    $artigo_tag->tag = $tag->tag;
                    $artigo_tag->tipo = 'A';
                    $artigo_tag->save();
                }
            }
            DB::commit();
            return $artigo;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($id_artigo, $input) {
        DB::beginTransaction();
        try {
            $artigo = $this->model->where('id_artigo', $id_artigo)->first();
            if (!$artigo) {
                abort(404);
            }
            if ($artigo->titulo != $input['titulo']) {
                // slugify
                $slug = UtilHelper::slugify($this->model, $input['titulo']);
                $artigo->slug = $slug;
            }
            $artigo->id_articulista = $input['articulista'];
            $artigo->id_arquivo = $input['foto'];
            $artigo->data_artigo = UtilHelper::formatDate($input['data'] . ' ' . $input['hora'], true);
            $artigo->chapeu = $input['chapeu'];
            $artigo->titulo = $input['titulo'];
            $artigo->complemento = $input['complemento'];
            $artigo->texto = $input['texto'];
            $artigo->publicado = isset($input['publicado']) ? $input['publicado'] : 'N';
            $artigo->save();
            
            // verifica se existem anexos
            if ($input['anexos']) {
                foreach (explode(",", $input['anexos']) as $id_arquivo) {
                    $artigo_anexo = ArtigoAnexoModel::where('id_artigo', $artigo->id_artigo)->where('id_arquivo', $id_arquivo)->first();
                    if (!$artigo_anexo) {
                        $anexo = new ArtigoAnexoModel();
                        $anexo->id_artigo = $artigo->id_artigo;
                        $anexo->id_arquivo = $id_arquivo;
                        $anexo->created_by = Auth::user()->id_usuario;
                        $anexo->save();
                    }
                }

                ArtigoAnexoModel::whereNotIn('id_arquivo', explode(",", $input['anexos']))->where('id_artigo', $artigo->id_artigo)->delete();
            } else {
                ArtigoAnexoModel::where('id_artigo', $artigo->id_artigo)->delete();
            }
            
            // verifica se tem tags
            TagConteudoModel::where('conteudo', $artigo->id_artigo)->where('tipo', 'A')->delete();
            if ($input['tags']) {
                foreach (explode(",", $input['tags']) as $mtag) {
                    // verifica se a tag existe
                    $tag = TagModel::where('tag', mb_strtolower(trim($mtag)))->first();
                    if (!$tag) {
                        $tag = new TagModel();
                        $slug = UtilHelper::slugify($tag, $mtag, false);
                        $tag->tag = mb_strtolower(trim($mtag));
                        $tag->slug = $slug;
                        $tag->save();
                    }
                    
                    $artigo_tag = new TagConteudoModel();
                    $artigo_tag->conteudo = $artigo->id_artigo;
                    $artigo_tag->tag = $tag->tag;
                    $artigo_tag->tipo = 'A';
                    $artigo_tag->save();
                }
            }
            
            DB::commit();
            return $artigo;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($id_artigo) {
        DB::beginTransaction();
        try {
            $artigo = $this->model->where('id_artigo', $id_artigo)->first();
            if (!$artigo) {
                abort(404);
            }
            $artigo->delete();
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

}
