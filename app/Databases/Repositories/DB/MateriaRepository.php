<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\MateriaModel;
use App\Databases\Models\MateriaAnexoModel;
use App\Databases\Models\TagModel;
use App\Databases\Models\TagConteudoModel;
use App\Helpers\UtilHelper;
use DB;
use Auth;
use Exception;

class MateriaRepository {

    private $model;

    public function __construct(MateriaModel $model) {
        $this->model = $model;
    }

    public function paginate($id_secao, $data) {

        $total_materias = $this->model->where('id_secao', $id_secao)->count();
        $total_filtrados = $total_materias;
        $materias = $this->model->with('usuario')->where('id_secao', $id_secao);

        if ($data['search']['value'] != '') {
            $materias->where('titulo', 'like', '%' . $data['search']['value'] . '%');
            $filtro = $materias;
            $total_filtrados = $filtro->count();
        }

        $orderIndex = $data['order'][0]['column'];
        $orderDirection = $data['order'][0]['dir'];
        $orderColumn = $data['columns'][$orderIndex]['data'];
        $materias->orderBy($orderColumn, $orderDirection);
        $materias->skip($data['start'])->take($data['length']);

        $items = [];
        foreach ($materias->get() as $materia) {
            $ts = new \DateTime($materia->data_materia);
            array_push($items, [
                'data_materia' => '<span data-order="' . $ts->format('U') . '">' . UtilHelper::formatDate($materia->data_materia, true) . '</span>',
                'titulo' => $materia->titulo,
                'created_by' => $materia->usuario->nome,
                'publicado' => '<center><span class="text-center ' . ($materia->publicado == 'S' ? 'text-success' : 'text-danger') . '"><i class="fa ' . ($materia->publicado == 'S' ? 'fa-check' : 'fa-close') . '"></i></span></center>',
                'acao' => '<table border="0" cellpadding="0" cellspacing="0"><tr style="background-color: inherit !important;"><td nowrap><a href="javascript:;" class="btn blue btn-edit" data-secao-id="' . $materia->id_secao . '" data-id="' . $materia->id_materia . '"><i class="fa fa-edit"></i></a>&nbsp;<a href="javascript:;" data-url="/secao/' . $materia->id_secao . '/materia/' . $materia->id_materia . '" class="btn red btn-destroy" data-id="' . $materia->id_materia . '"  data-message="Deseja realmente excluir esta matéria?" data-success-message="Matéria excluída com sucesso!"><i class="fa fa-trash"></i></a></td></tr></table>'
            ]);
        }

        $result = [
            'draw' => $data['draw'],
            'recordsFiltered' => $total_filtrados,
            'recordsTotal' => $total_materias,
            'data' => $items
        ];
        return $result;
    }

    public function get($id_secao, $id_materia) {
        return $this->model->where('id_secao', $id_secao)->where('id_materia', $id_materia)->first();
    }

    public function getAnexos($id_materia) {
        return MateriaAnexoModel::where('id_materia', $id_materia)->get();
    }

    public function getTags($id_materia) {
        return TagConteudoModel::where('conteudo', $id_materia)->where('tipo', 'M')->orderBy('tag', 'asc')->get();
    }

    public function create($data, $id_secao) {
        DB::beginTransaction();
        try {
            $materia = $this->model;

            // slugify
            $slug = UtilHelper::slugify($this->model, $data['titulo']);
            $materia->id_secao = $id_secao;
            $materia->data_materia = UtilHelper::formatDate($data['data'] . ' ' . $data['hora'], true);
            $materia->chapeu = $data['chapeu'];
            $materia->titulo = $data['titulo'];
            $materia->slug = $slug;
            $materia->complemento = $data['complemento'];
            $materia->autor_nome = isset($data['autor_nome']) ? $data['autor_nome'] : null;
            $materia->autor_email = isset($data['autor_email']) ? $data['autor_email'] : null;
            $materia->autor_local = isset($data['autor_local']) ? $data['autor_local'] : null;
            $materia->texto = $data['texto'];
            $materia->publicado = $data['publicado'];
            $materia->created_by = Auth::user()->id_usuario;
            $materia->save();

            // verifica se existem anexos
            if ($data['anexos']) {
                foreach (explode(",", $data['anexos']) as $id_arquivo) {
                    $anexo = new MateriaAnexoModel();
                    $anexo->id_materia = $materia->id_materia;
                    $anexo->id_arquivo = $id_arquivo;
                    $anexo->created_by = Auth::user()->id_usuario;
                    $anexo->save();
                }
            }

            // verifica se tem tags
            if ($data['tags']) {
                foreach (explode(",", $data['tags']) as $mtag) {
                    // verifica se a tag existe
                    $tag = TagModel::where('tag', mb_strtolower(trim($mtag)))->first();
                    if (!$tag) {
                        $tag = new TagModel();
                        $slug = UtilHelper::slugify($tag, $mtag, false);
                        $tag->tag = mb_strtolower(trim($mtag));
                        $tag->slug = $slug;
                        $tag->save();
                    }

                    $materia_tag = new TagConteudoModel();
                    $materia_tag->conteudo = $materia->id_materia;
                    $materia_tag->tag = $tag->tag;
                    $materia_tag->tipo = 'M';
                    $materia_tag->save();
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }

        return $materia;
    }

    public function update($data, $id_secao, $id_materia) {
        DB::beginTransaction();

        try {
            $materia = $this->model->where('id_secao', $id_secao)->where('id_materia', $id_materia)->first();
            if (!$materia) {
                abort(404);
            }

            if ($materia->titulo != $data['titulo']) {
                $slug = UtilHelper::slugify($this->model, $data['titulo']);
                $materia->slug = $slug;
            }

            $materia->data_materia = UtilHelper::formatDate($data['data'] . ' ' . $data['hora'], true);
            $materia->chapeu = $data['chapeu'];
            $materia->titulo = $data['titulo'];
            $materia->complemento = $data['complemento'];
            $materia->autor_nome = isset($data['autor_nome']) ? $data['autor_nome'] : null;
            $materia->autor_email = isset($data['autor_email']) ? $data['autor_email'] : null;
            $materia->autor_local = isset($data['autor_local']) ? $data['autor_local'] : null;
            $materia->texto = $data['texto'];
            $materia->publicado = isset($data['publicado']) ? 'S' : 'N';
            $materia->save();

            // verifica se existem anexos
            if ($data['anexos']) {
                foreach (explode(",", $data['anexos']) as $id_arquivo) {
                    $materia_anexo = MateriaAnexoModel::where('id_materia', $materia->id_materia)->where('id_arquivo', $id_arquivo)->first();
                    if (!$materia_anexo) {
                        $anexo = new MateriaAnexoModel();
                        $anexo->id_materia = $materia->id_materia;
                        $anexo->id_arquivo = $id_arquivo;
                        $anexo->created_by = Auth::user()->id_usuario;
                        $anexo->save();
                    }
                }

                MateriaAnexoModel::whereNotIn('id_arquivo', explode(",", $data['anexos']))->where('id_materia', $materia->id_materia)->delete();
            } else {
                MateriaAnexoModel::where('id_materia', $materia->id_materia)->delete();
            }

            // verifica se tem tags
            TagConteudoModel::where('conteudo', $materia->id_materia)->where('tipo', 'M')->delete();
            if ($data['tags']) {
                foreach (explode(",", $data['tags']) as $mtag) {
                    // verifica se a tag existe
                    $tag = TagModel::where('tag', mb_strtolower(trim($mtag)))->first();
                    if (!$tag) {
                        $tag = new TagModel();
                        $slug = UtilHelper::slugify($tag, $mtag, false);
                        $tag->tag = mb_strtolower(trim($mtag));
                        $tag->slug = $slug;
                        $tag->save();
                    }

                    $materia_tag = new TagConteudoModel();
                    $materia_tag->conteudo = $materia->id_materia;
                    $materia_tag->tag = $tag->tag;
                    $materia_tag->tipo = 'M';
                    $materia_tag->save();
                }
            }

            DB::commit();
            return $materia;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data, $id_secao, $id_materia) {
        DB::beginTransaction();
        try {
            $materia = $this->model->where('id_secao', $id_secao)->where('id_materia', $id_materia)->first();
            if (!$materia) {
                abort(404);
            }
            $materia->delete();
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            throw new Exception($ex->getMessage());
        }
    }

}
