<?php

namespace App\Databases\Repositories\DB;

use App\Databases\Models\PaginaLinkModel;

class PaginaLinkRepository {

    private $model;

    public function __construct(PaginaLinkModel $model) {
        $this->model = $model;
    }
    
    public function getAll($id_pagina) {
        return $this->model->where('id_pagina', $id_pagina)->get();
    }
    
    public function destroyAll($id_pagina) {
        return $this->model->where('id_pagina', $id_pagina)->delete();
    }
}